package com.trablone.basket.service;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import com.trablone.basket.MainActivity;
import com.trablone.basket.R;

public class NotificationService extends Service {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        String message = intent.getStringExtra("message");
        sendNotification(this, 0, message);
        return Service.START_NOT_STICKY;
    }

    private NotificationManager notificationManager;
    private static final String channelId = "vk_channel";
    private static final String channelName = "Channel vk";
    private NotificationChannel mChannel;

    private void sendNotification(Context context, int id, String message) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);


        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= 26 ) {
            this.mChannel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_DEFAULT);
            this.mChannel.enableVibration(false);
            this.mChannel.setVibrationPattern(null);
            this.mChannel.enableLights(false);
            this.mChannel.setSound(null, null);
            this.notificationManager.createNotificationChannel(this.mChannel);
        }
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, channelId);

        builder.setLargeIcon(BitmapFactory.decodeResource(getResources(),R.mipmap.ic_launcher));
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setContentTitle(getResources().getString(R.string.app_name));
        builder .setContentText(message);
        builder.setAutoCancel(true);
        builder.setSound(defaultSoundUri);
        builder.setContentIntent(contentIntent);


        startForeground(id, builder.build());
    }
}
