package com.trablone.basket;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.trablone.basket.adapters.OrderHistoryAdapter;
import com.trablone.basket.model.OrderHistrory;
import com.trablone.basket.model.User;
import com.trablone.basket.response.ObjectResponse;
import com.trablone.basket.rest.GetOrderHistory;
import com.trablone.basket.viewModel.OrderHistoryViewModel;
import com.trablone.basket.viewModel.UserViewModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderHistoryActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.recycler_product)
    RecyclerView recyclerView;

    public StaggeredGridLayoutManager mLayoutManager;
    private OrderHistoryAdapter adapter;
    private int pastVisiblesItems;

    private boolean refresh;

    public void setRefresh(boolean refresh) {
        this.refresh = refresh;
    }

    public boolean isRefresh() {
        return refresh;
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("История заказов");

        viewModel = ViewModelProviders.of(this).get(OrderHistoryViewModel.class);

        adapter = new OrderHistoryAdapter(this);
        recyclerView.setAdapter(adapter);
        mLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(mLayoutManager);

        viewModel.getOrders(10, 0).observeForever(objectResponse -> {
            GetOrderHistory orderHistory = (GetOrderHistory)objectResponse.getObject();
            List<OrderHistrory> list = orderHistory.getOrders();
            adapter.setList(list);
        });

    }

    private RecyclerView.OnScrollListener onScroll = new RecyclerView.OnScrollListener() {

        int visibleItemCount, totalItemCount;

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);

        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);


            visibleItemCount = mLayoutManager.getChildCount();
            totalItemCount = mLayoutManager.getItemCount();
            int[] firstVisibleItems = null;
            firstVisibleItems = mLayoutManager.findFirstVisibleItemPositions(firstVisibleItems);
            if (firstVisibleItems != null && firstVisibleItems.length > 0) {
                pastVisiblesItems = firstVisibleItems[0];
            }
                if ((visibleItemCount + pastVisiblesItems) >= totalItemCount - 1) {

                    if (!isRefresh()) {
                        setRefresh(true);
                        viewModel.getOrders(10, adapter.getItemCount()).observeForever(objectResponse -> {
                            GetOrderHistory orderHistory = (GetOrderHistory)objectResponse.getObject();
                            List<OrderHistrory> list = orderHistory.getOrders();
                            adapter.addList(list);
                        });
                    }
                }
        }
    };

    private OrderHistoryViewModel viewModel;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}