package com.trablone.basket.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.trablone.basket.model.Auto;
import com.trablone.basket.model.Order;

import java.util.List;

@Dao
public abstract class OrderDao {

    @Query("SELECT * FROM `order`")
    public abstract LiveData<List<Order>> getOrders();

    @Query("SELECT * FROM `order` WHERE order_id =:id")
    public abstract LiveData<Order> getOrder(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void bulkInsert(Order... wallpapers);

    @Update
    public abstract void update(Order... wallpaper);

    @Delete
    public abstract void delete(Order... wallpaper);

    @Query("DELETE FROM `order`")
    public abstract void delete();
}
