package com.trablone.basket.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.trablone.basket.model.Auto;
import com.trablone.basket.model.Card;

import java.util.List;

@Dao
public abstract class AutoDao {

    @Query("SELECT * FROM auto  ORDER BY id DESC")
    public abstract LiveData<List<Auto>> getAutos();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void bulkInsert(Auto... wallpapers);

    @Update
    public abstract void update(Auto... wallpaper);

    @Delete
    public abstract void delete(Auto... wallpaper);

    @Query("DELETE FROM auto")
    public abstract void delete();
}
