package com.trablone.basket.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.trablone.basket.model.Product;

import java.util.List;

@Dao
public abstract class ProductDao {


    @Query("SELECT * FROM product  ORDER BY id DESC")
    public abstract LiveData<List<Product>> getAllProducts();

    @Query("SELECT * FROM product WHERE category = :category AND company = :company ORDER BY id DESC")
    public abstract LiveData<List<Product>> getProductsCompanyCategory(int category, int company);

    @Query("SELECT * FROM product WHERE company = :company AND count > 0 ORDER BY id DESC")
    public abstract LiveData<List<Product>> getProductsCompanyCategoryBasket(int company);

    @Query("SELECT * FROM product WHERE id = :id")
    public abstract Product getProduct(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void bulkInsert(Product... wallpaper);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void bulkInsert(List<Product> wallpapers);

    @Delete
    public abstract void bulkDelete(Product... wallpaper);

    @Update
    public abstract void update(Product... wallpaper);

    @Query("SELECT * FROM product WHERE id = :id")
    public abstract LiveData<List<Product>> getWallpaper(int id);

    @Query("DELETE FROM product")
    public abstract void delete();
}
