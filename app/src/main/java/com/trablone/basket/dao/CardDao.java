package com.trablone.basket.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.trablone.basket.model.Auto;
import com.trablone.basket.model.Card;

import java.util.List;

@Dao
public abstract class CardDao {
    @Query("SELECT * FROM card  ORDER BY id DESC")
    public abstract LiveData<List<Card>> getCards();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void bulkInsert(Card... wallpapers);

    @Update
    public abstract void update(Card... wallpaper);

    @Delete
    public abstract void delete(Card... wallpaper);

    @Query("DELETE FROM card")
    public abstract void delete();
}
