package com.trablone.basket.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.trablone.basket.model.Auto;
import com.trablone.basket.model.User;

import java.util.List;

@Dao
public abstract class UserDao {

    @Query("SELECT * FROM user WHERE id = 1")
    public abstract LiveData<User> getUser();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void bulkInsert(User... wallpapers);

    @Delete
    public abstract void bulkDelete(User... wallpaper);

    @Update
    public abstract void update(User... wallpaper);

    @Query("DELETE FROM user")
    public abstract void delete();
}
