package com.trablone.basket.location;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;


/**
 * Created by trablone on 7/2/17.
 */

public class LocationService {

    private static LocationService INSTANCE = null;
    private LocationCallback mLocationCallback;
    private LocationRequest mLocationRequest;
    private FusedLocationProviderClient mFusedLocationClient;
    private Location location;
    private boolean isInit = false;

    private Context context;

    public LocationService(Context context) {
        INSTANCE = this;
        this.context = context;
    }

    public static LocationService getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new LocationService(context);
        }


        return INSTANCE;
    }

    public boolean isInit() {
        return isInit;
    }

    public void setInit(boolean init) {
        isInit = init;
    }

    public void init(final Context context) {
        if (!isInit) {
            try {
                mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context);
                mLocationCallback = new LocationCallback() {
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        if (locationResult != null) {
                            location = locationResult.getLastLocation();
                            if (location != null) {
                                Log.e("tr", "service location: " + location.toString());
                                Intent intent = new Intent("services_location");
                                intent.putExtra("location", location);
                                context.sendBroadcast(intent);
                                //sendLocation(location);
                            }
                        }
                    }
                };

                mLocationRequest = new LocationRequest();
                mLocationRequest.setInterval(1000* 60);
                mLocationRequest.setFastestInterval(10000);
                mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                if (mFusedLocationClient != null)
                    mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null);
                isInit = true;
            } catch (Throwable e) {
                Log.e("tr", "Service: " + e.getMessage());
            }
        }
    }

    public void sendLocation(Location location) {
        /*BaseActivity activity = (BaseActivity)context;
        if (activity != null){
            RequestParams params = new RequestParams();
            params.put("user_id", activity.getUser().id);
            params.put("x", location.getLatitude());
            params.put("y", location.getLongitude());

            HttpClient.post("/user/setcoords", context, params, new JsonHttpResponseHandler() {

            });
        }
        */

    }
    public Location getLocation() {
        return location;
    }


    public void onDestroy() {
        isInit = false;
        if (mFusedLocationClient != null)
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        Log.e("tr", "Location onDestroy");

    }


}
