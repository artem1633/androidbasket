package com.trablone.basket.rest;

import com.trablone.basket.model.Product;

import java.util.List;

public class GetProduct extends BaseModel {

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    private List<Product> productList;
}
