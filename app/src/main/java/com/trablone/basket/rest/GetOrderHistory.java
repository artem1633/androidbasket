package com.trablone.basket.rest;

import com.trablone.basket.model.OrderHistrory;

import java.util.List;

public class GetOrderHistory extends BaseModel {

    public List<OrderHistrory> getOrders() {
        return orders;
    }

    public void setOrders(List<OrderHistrory> orders) {
        this.orders = orders;
    }

    private List<OrderHistrory> orders;
}
