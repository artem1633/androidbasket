package com.trablone.basket.rest;

public class GetOrderStatus extends BaseModel {

    public int getOrder_status() {
        return order_status;
    }

    public void setOrder_status(int order_status) {
        this.order_status = order_status;
    }

    private int order_status;
}
