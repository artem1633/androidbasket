package com.trablone.basket.rest;

import com.trablone.basket.model.Order;

public class GetCreateOrder extends BaseModel {

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    private Order order;
}
