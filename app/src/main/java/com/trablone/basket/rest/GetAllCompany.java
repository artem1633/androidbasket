package com.trablone.basket.rest;

import com.trablone.basket.model.Company;

import java.util.List;
import java.util.Locale;

public class GetAllCompany extends BaseModel {

    public List<Company> getCompanyList() {
        return companyList;
    }

    public void setCompanyList(List<Company> companyList) {
        this.companyList = companyList;
    }

    private List<Company> companyList;
}
