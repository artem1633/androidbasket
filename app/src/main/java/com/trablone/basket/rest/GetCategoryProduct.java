package com.trablone.basket.rest;

import com.trablone.basket.model.CategoryCompany;

import java.util.List;

public class GetCategoryProduct extends BaseModel {


    public List<CategoryCompany> getCategoryProductList() {
        return categoryProductList;
    }

    public void setCategoryProductList(List<CategoryCompany> categoryProductList) {
        this.categoryProductList = categoryProductList;
    }

    private List<CategoryCompany> categoryProductList;
}
