package com.trablone.basket.rest;

public class GetMerchant extends BaseModel {

    public String getMERCHANT_PUBLIC_ID() {
        return MERCHANT_PUBLIC_ID;
    }

    public void setMERCHANT_PUBLIC_ID(String MERCHANT_PUBLIC_ID) {
        this.MERCHANT_PUBLIC_ID = MERCHANT_PUBLIC_ID;
    }

    public String getMERCHANT_API_PASS() {
        return MERCHANT_API_PASS;
    }

    public void setMERCHANT_API_PASS(String MERCHANT_API_PASS) {
        this.MERCHANT_API_PASS = MERCHANT_API_PASS;
    }

    private String MERCHANT_PUBLIC_ID;
    private String MERCHANT_API_PASS;
}
