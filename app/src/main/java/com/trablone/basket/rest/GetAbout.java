package com.trablone.basket.rest;

public class GetAbout extends BaseModel {

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    private String about;
}
