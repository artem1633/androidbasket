package com.trablone.basket.rest;

import com.trablone.basket.App;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

public interface RetrofitApi {


    @GET("api/client/get-all-company")
    Call<GetAllCompany> getAllCompany(@Query("access") String key);

    @GET("api/client/support")
    Call<GetSupport> getSupport(@Query("access") String key);

    @GET("api/client/about")
    Call<GetAbout> getAbout(@Query("access") String key);

    @GET("api/client/get-company-by-category")
    Call<GetAllCompany> getAllCompanyCategory(@Query("access") String key, @Query("category_id") int category_id);

    @GET("api/client/get-product-by-category")
    Call<GetProduct> getProductCategory(@Query("access") String key, @Query("category_product_id") int category_id, @Query("company_id") int company_id);

    @GET("api/client/get-category-product")
    Call<GetCategoryProduct> getCategoryProductCompany(@Query("access") String key, @Query("company_id") int company_id);

    @GET("api/client/get-category-company")
    Call<GetCategoryCompany> getCategoryCompany(@Query("access") String key);

    @GET("api/client/register")
    Call<GetRegister> register(@Query("name") String name, @Query("phone") String phone);

    @Headers("Content-Type: application/json")
    @GET("api/client/add-order")
    Call<GetCreateOrder> createOrder(@Query("access") String access, @Query("company_id") int company_id, @Query("productlist") String productlist, @QueryMap HashMap<String, String> map);

    @GET("api/client/confirm-register")
    Call<GetConfirmRegister> confirmRegister(@Query("phone") String phone);

    @GET("api/client/confirm-register")
    Call<GetConfirmRegister> confirmRegister(@Query("sms") String name, @Query("phone") String phone, @Query("key_push") String token);

    @GET("api/client/get-orders-history")
    Call<GetOrderHistory> getOrderHistory(@Query("access") String access, @Query("offset") int offset, @Query("limit") int limit);

    @GET("api/client/get-order-status")
    Call<GetOrderStatus> getOrderStatus(@Query("access") String key, @Query("order_id") int order_id);

    @GET("api/client/client-geo")
    Call<GetGeo> getGeo(@Query("access") String key, @Query("coord_x") String x, @Query("coord_y") String y);

    @GET("api/client/get-merchant")
    Call<GetMerchant> getMerchant(@Query("access") String key);

    @GET
    Call<ResponseBody> downloadFile(@Url String fileUrl);

    @GET
    Call<JSONObject> getRoutes(@Url String fileUrl, @QueryMap HashMap<String, String> map);

}
