package com.trablone.basket.rest;

public class GetSupport extends BaseModel {

    public String getSupport() {
        return support;
    }

    public void setSupport(String support) {
        this.support = support;
    }

    private String support;
}
