package com.trablone.basket.rest;

import com.trablone.basket.model.CategoryCompany;

import java.util.List;

public class GetCategoryCompany extends BaseModel {


    public List<CategoryCompany> getCategoryCompanyList() {
        return categoryCompanyList;
    }

    public void setCategoryCompanyList(List<CategoryCompany> categoryCompanyList) {
        this.categoryCompanyList = categoryCompanyList;
    }

    private List<CategoryCompany> categoryCompanyList;
}
