package com.trablone.basket.response;



public class ObjectResponse extends BaseResponse {
    private Object object;

    public ObjectResponse(Object object) {
        this.object = object;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }
}
