package com.trablone.basket.response;


import java.util.ArrayList;
import java.util.List;


public class ListResponse extends BaseResponse {
    private List<?> list = new ArrayList<>();

    public ListResponse(List<?> list) {
        this.list = list;
    }

    public List<?> getList() {
        return list;
    }

    public void setList(List<?> list) {
        this.list = list;
    }
}
