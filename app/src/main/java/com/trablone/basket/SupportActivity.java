package com.trablone.basket;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;

import com.trablone.basket.response.ObjectResponse;
import com.trablone.basket.rest.GetSupport;
import com.trablone.basket.viewModel.SupportViewModel;
import com.trablone.basket.viewModel.UserViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SupportActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.item_web)
    WebView webView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Поддержка");
        viewModel = ViewModelProviders.of(this).get(SupportViewModel.class);

        viewModel.getSupport().observeForever(new Observer<ObjectResponse>() {
            @Override
            public void onChanged(@Nullable ObjectResponse objectResponse) {
                GetSupport support = (GetSupport)objectResponse.getObject();
                if (support != null){
                    webView.loadDataWithBaseURL("", support.getSupport(), "text/html", "utf-8", null);
                }
            }
        });
    }

    private SupportViewModel viewModel;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}