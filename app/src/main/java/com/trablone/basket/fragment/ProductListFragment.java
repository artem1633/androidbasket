package com.trablone.basket.fragment;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.trablone.basket.BasketActivity;
import com.trablone.basket.CompanyActivity;
import com.trablone.basket.R;
import com.trablone.basket.adapters.ProductAdapter;
import com.trablone.basket.model.Company;
import com.trablone.basket.model.Product;
import com.trablone.basket.response.ListResponse;
import com.trablone.basket.response.ObjectResponse;
import com.trablone.basket.rest.GetProduct;
import com.trablone.basket.viewModel.ProductViewModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductListFragment extends Fragment {

    private CompanyActivity activity;

    public static ProductListFragment getInstance(int category_id, int company_id){
        ProductListFragment fragment = new ProductListFragment();
        Bundle params = new Bundle();
        params.putInt("category_id", category_id);
        params.putInt("company_id", company_id);
        fragment.setArguments(params);

        return fragment;
    }


    private ProductViewModel viewModel;
    private int category_id;
    private int company_id;

    @BindView(R.id.recycler_product)
    RecyclerView recyclerView;

    private ProductAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_list, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);

        activity = (CompanyActivity)getActivity();
        viewModel = ViewModelProviders.of(this).get(ProductViewModel.class);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        adapter = new ProductAdapter(getActivity(), new ProductAdapter.ProductListener() {
            @Override
            public void onUpdate(Product item) {
                item.setCategory(category_id);
                item.setCompany(company_id);
                viewModel.updateProduct(item);
                activity.updateBasket();
            }

        });

        recyclerView.setAdapter(adapter);

        category_id = getArguments().getInt("category_id");
        company_id = getArguments().getInt("company_id");

    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.getProducts(category_id, company_id).observe(this, new Observer<ObjectResponse>() {
            @Override
            public void onChanged(@Nullable ObjectResponse objectResponse) {
                if (objectResponse.getObject() != null){

                    GetProduct getProduct = (GetProduct)objectResponse.getObject();
                    List<Product> list = getProduct.getProductList();
                    Log.e("tr", "count fragment " + list.size());
                    adapter.setList(list);

                }
            }
        });
    }


}
