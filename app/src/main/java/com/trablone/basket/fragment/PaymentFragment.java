package com.trablone.basket.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.wallet.AutoResolveHelper;
import com.google.android.gms.wallet.PaymentData;
import com.google.android.gms.wallet.PaymentDataRequest;
import com.google.android.gms.wallet.PaymentMethodToken;
import com.google.android.gms.wallet.PaymentsClient;
import com.google.android.gms.wallet.TransactionInfo;
import com.trablone.basket.MainActivity;
import com.trablone.basket.R;
import com.trablone.basket.adapters.CardAdapter;
import com.trablone.basket.model.Auto;
import com.trablone.basket.model.Card;
import com.trablone.basket.model.Company;
import com.trablone.basket.model.Order;
import com.trablone.basket.model.Product;
import com.trablone.basket.model.User;
import com.trablone.basket.payment.PaymentHelper;
import com.trablone.basket.payment.api.Api;
import com.trablone.basket.payment.api.models.Transaction;
import com.trablone.basket.payment.api.response.ApiError;
import com.trablone.basket.payment.d3s.D3SDialog;
import com.trablone.basket.payment.d3s.D3SDialogListener;
import com.trablone.basket.payment.googlepay.Constants;
import com.trablone.basket.payment.googlepay.PaymentsUtil;
import com.trablone.basket.rest.GetCreateOrder;
import com.trablone.basket.rest.GetMerchant;
import com.trablone.basket.viewModel.UserViewModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class PaymentFragment extends Fragment {

    public static PaymentFragment getInstance(Auto auto){
        PaymentFragment fragment = new PaymentFragment();
        Bundle params = new Bundle();
        params.putSerializable("auto", auto);
        fragment.setArguments(params);

        return fragment;
    }
    private UserViewModel viewModel;
    private User user;
    private Auto auto;

    private CardAdapter autoAdapter;
    @BindView(R.id.recycler_payment)
    RecyclerView recyclerView;


    @BindView(R.id.pwg_button)
    View mPwgButton;

    @BindView(R.id.pwg_status)
    TextView mPwgStatusText;

    private List<Card> list;


    @Override
    public void onDestroy() {
        paymentHelper.onDestroy();
        super.onDestroy();
    }
    int price;

    private PaymentHelper paymentHelper;
    private Company company;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(this).get(UserViewModel.class);

        price = getActivity().getIntent().getIntExtra("price", 0);
        company = (Company) getActivity().getIntent().getSerializableExtra("company");
        auto = (Auto) getArguments().getSerializable("auto");

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        autoAdapter = new CardAdapter(getActivity(), new CardAdapter.AutoListener() {
            @Override
            public void addAuto(Card auto) {
                viewModel.insertCard(auto);
                hideKeyboard();
                getCards();
            }

            @Override
            public void deleteAuto(Card auto) {
                viewModel.deleteCard(auto);
                getCards();
            }
        });

        recyclerView.setAdapter(autoAdapter);
        viewModel.getUser().observeForever(objectResponse -> {
            if (objectResponse.getObject() != null){
                user = (User)objectResponse.getObject();
            }
        });

        viewModel.getMerchant().observeForever(objectResponse -> {
            if (objectResponse.getObject() != null){
                paymentHelper = new PaymentHelper(getActivity());
                GetMerchant getMerchant = (GetMerchant) objectResponse.getObject();
                paymentHelper.setMERCHANT_API_PASS(getMerchant.getMERCHANT_API_PASS());
                paymentHelper.setMERCHANT_PUBLIC_ID(getMerchant.getMERCHANT_PUBLIC_ID());
                getCards();


                paymentHelper.setListener(new PaymentHelper.CheckPayAvailable() {
                    @Override
                    public void onSuccess() {
                        mPwgStatusText.setVisibility(View.GONE);
                        mPwgButton.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onFailure() {
                        mPwgStatusText.setText(R.string.pwg_status_unavailable);
                    }

                    @Override
                    public void onCreateOrder() {
                        createOrder();
                    }
                });
                paymentHelper.setPrice(price);
                paymentHelper.checkIsReadyToPay();
            }
        });
    }

    private void getCards(){
        viewModel.getCards().observeForever(listResponse -> {
            if (listResponse.getList() != null){
                list = (List<Card>)listResponse.getList();
                autoAdapter.setList(list);
            }
        });
    }


    @OnClick(R.id.pwg_button)
    public void requestPayment() {
        mPwgButton.setClickable(false);
        paymentHelper.requestPayment();
    }
    @OnClick(R.id.button_pay)
    public void payToCard(){
        //createOrder();
        if (list != null && list.size() > 0){
            Card card = autoAdapter.getSelectAuto();
            paymentHelper.auth(card.getHash(), card.getHolderName(), price);
        }else {
            Snackbar.make(recyclerView, "Пожалуйста добавте карту", Snackbar.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        paymentHelper.onActivityResult(requestCode, resultCode, data);
        mPwgButton.setClickable(true);
    }

    private void createOrder(){
        viewModel.getProductBasket(company.getId()).observeForever(listResponse -> {
            List<Product> list = (List<Product>)listResponse.getList();
            if (list.size() > 0){
                JSONArray array = new JSONArray();
                for (Product product : list){
                    JSONObject object = new JSONObject();
                    try {
                        object.put("product_id", String.valueOf(product.getId()));
                        object.put("amount", String.valueOf(product.getCount()));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    array.put(object);
                }

                HashMap<String, String> map = new HashMap<>();

                map.put("default", "1");

                map.put("car_type", auto.getAutoMark());
                map.put("car_color", auto.getAutoColor());
                if (!TextUtils.isEmpty(auto.getAutoModel()))
                    map.put("car_model", auto.getAutoModel());
                map.put("car_number", String.valueOf(auto.getAutoNumber()));

                viewModel.createOrder(company.getId(), array.toString(), map).observe(this, objectResponse -> {
                    if (objectResponse.getObject() != null && getActivity() != null){
                        GetCreateOrder createOrder = (GetCreateOrder)objectResponse.getObject();
                        if (createOrder.isStatus()){
                            Order order1 = createOrder.getOrder();
                            order1.setCompany_id(company.getId());
                            order1.setCompany_image(company.getPicture());
                            order1.setCompany_name(company.getName());
                            order1.setCompany_x(company.getCoord_x());
                            order1.setCompany_y(company.getCoord_y());
                            Log.e("tr", "time " + order1.getTime_max() + " summ " + order1.getSumma());

                            viewModel.insertOrder(order1);

                            for (Product product : list){
                                product.setCount(0);
                                product.setCompany(0);
                                product.setCategory(0);
                                viewModel.updateProduct(product);
                            }



                            new AlertDialog.Builder(getActivity())
                                    .setTitle(R.string.app_name)
                                    .setCancelable(false)
                                    .setMessage("Ваш заказ отправлен на обработку")
                                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                            MainActivity.showActivity(getContext());
                                            getActivity().finish();
                                        }
                                    })
                                    .show();

                        }else {
                            new AlertDialog.Builder(getActivity())
                                    .setTitle(R.string.app_name)
                                    .setCancelable(false)
                                    .setMessage("Ошибка создания заказа\n" + createOrder.getErrors())
                                    .setPositiveButton("Повторить", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            createOrder();
                                        }
                                    })
                                    .show();
                        }
                    }else if (getActivity() != null){
                        new AlertDialog.Builder(getActivity())
                                .setTitle(R.string.app_name)
                                .setCancelable(false)
                                .setMessage("Ошибка создания заказа")
                                .setPositiveButton("Повторить", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        createOrder();
                                    }
                                })
                                .show();
                    }
                });
            }
        });
    }



    protected void hideKeyboard() {
        try {
            View currentFocus = getActivity().getCurrentFocus();
            if (currentFocus != null) {
                InputMethodManager inputMethodManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (inputMethodManager != null) {
                    inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 0);
                }
            }
        } catch (Exception e) {

        }
    }
}
