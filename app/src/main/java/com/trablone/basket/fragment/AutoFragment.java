package com.trablone.basket.fragment;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.trablone.basket.ActivityConfirmOrder;
import com.trablone.basket.R;
import com.trablone.basket.adapters.AutoAdapter;
import com.trablone.basket.model.Auto;
import com.trablone.basket.model.Card;
import com.trablone.basket.model.User;
import com.trablone.basket.response.ListResponse;
import com.trablone.basket.response.ObjectResponse;
import com.trablone.basket.viewModel.UserViewModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AutoFragment extends Fragment {



    private UserViewModel viewModel;
    private User user;
    @BindView(R.id.item_next)
    TextView textNext;
    private AutoAdapter autoAdapter;
    @BindView(R.id.recycler_auto)
    RecyclerView recyclerView;

    private List<Auto> list;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_auto, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(this).get(UserViewModel.class);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        autoAdapter = new AutoAdapter(getActivity(), new AutoAdapter.AutoListener() {
            @Override
            public void addAuto(Auto auto) {
                auto.setUserId(user.getId());
                viewModel.insertAuto(auto);
                hideKeyboard();
                getAutos();
            }

            @Override
            public void deleteAuto(Auto auto) {
                viewModel.deleteAuto(auto);
            }
        });
        recyclerView.setAdapter(autoAdapter);
        viewModel.getUser().observeForever(objectResponse -> {
            if (objectResponse.getObject() != null){
                user = (User)objectResponse.getObject();
            }
        });

        getAutos();
    }

    private void getAutos(){
        viewModel.getAutos().observeForever(listResponse -> {
            if (listResponse.getList() != null){
                list = (List<Auto>)listResponse.getList();
                autoAdapter.setList(list);
            }

        });
    }

    protected void hideKeyboard() {
        try {
            View currentFocus = getActivity().getCurrentFocus();
            if (currentFocus != null) {
                InputMethodManager inputMethodManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (inputMethodManager != null) {
                    inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 0);
                }
            }
        } catch (Exception e) {

        }
    }

    @OnClick(R.id.item_next)
    public void next(){
        if (list != null && list.size() > 0){
            Auto card = autoAdapter.getSelectAuto();
            ActivityConfirmOrder activityConfirmOrder = (ActivityConfirmOrder)getActivity();
            activityConfirmOrder.next(card);
        }else {
            Snackbar.make(recyclerView, "Пожалуйста добавте автомобиль", Snackbar.LENGTH_SHORT).show();
        }

    }

}
