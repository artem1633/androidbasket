package com.trablone.basket.fragment;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.trablone.basket.CompanyActivity;
import com.trablone.basket.MainActivity;
import com.trablone.basket.R;
import com.trablone.basket.adapters.CategoryCompanyAdapter;
import com.trablone.basket.di.MainModule;
import com.trablone.basket.model.CategoryCompany;
import com.trablone.basket.model.Company;
import com.trablone.basket.model.Order;
import com.trablone.basket.response.ObjectResponse;
import com.trablone.basket.rest.GetAllCompany;
import com.trablone.basket.rest.GetCategoryCompany;
import com.trablone.basket.rest.GetOrderStatus;
import com.trablone.basket.viewModel.MainViewModel;
import com.yandex.mapkit.MapKitFactory;
import com.yandex.mapkit.directions.DirectionsFactory;
import com.yandex.mapkit.directions.driving.DrivingArrivalPoint;
import com.yandex.mapkit.directions.driving.DrivingOptions;
import com.yandex.mapkit.directions.driving.DrivingRoute;
import com.yandex.mapkit.directions.driving.DrivingRouter;
import com.yandex.mapkit.directions.driving.DrivingSession;
import com.yandex.mapkit.directions.driving.Event;
import com.yandex.mapkit.directions.driving.JamSegment;
import com.yandex.mapkit.directions.driving.RequestPoint;
import com.yandex.mapkit.directions.driving.RequestPointType;
import com.yandex.mapkit.geometry.Geo;
import com.yandex.mapkit.geometry.Point;
import com.yandex.mapkit.geometry.Polyline;
import com.yandex.mapkit.layers.ObjectEvent;
import com.yandex.mapkit.map.CameraPosition;
import com.yandex.mapkit.map.MapObject;
import com.yandex.mapkit.map.MapObjectCollection;
import com.yandex.mapkit.map.MapObjectTapListener;
import com.yandex.mapkit.map.PlacemarkMapObject;
import com.yandex.mapkit.map.PolylineMapObject;
import com.yandex.mapkit.mapview.MapView;
import com.yandex.mapkit.user_location.UserLocationLayer;
import com.yandex.mapkit.user_location.UserLocationObjectListener;
import com.yandex.mapkit.user_location.UserLocationView;
import com.yandex.runtime.Error;
import com.yandex.runtime.image.ImageProvider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CompanyYandexFragment extends Fragment implements DrivingSession.DrivingRouteListener, UserLocationObjectListener {

    private final String MAPKIT_API_KEY = "9a088961-4b2a-44a7-88e0-c02f8a3e5d37";

    private final Point ROUTE_START_LOCATION = new Point(55.574257, 37.345048);
    private final Point ROUTE_END_LOCATION = new Point(55.915872, 37.838060);
    private final Point MOSCOW = new Point(
            (ROUTE_START_LOCATION.getLatitude() + ROUTE_END_LOCATION.getLatitude()) / 2,
            (ROUTE_START_LOCATION.getLongitude() + ROUTE_END_LOCATION.getLongitude()) / 2);
    @BindView(R.id.mapview)
    MapView mapView;

    @BindView(R.id.item_company)
    LinearLayout layoutCompany;
    @BindView(R.id.item_image)
    ImageView imageView;
    @BindView(R.id.item_title)
    TextView textView;
    @BindView(R.id.item_menu)
    TextView textMenu;
    @BindView(R.id.item_desc)
    TextView textDesc;
    @BindView(R.id.item_distance)
    TextView textDistance;
    @BindView(R.id.item_close)
    ImageView itemClose;
    @BindView(R.id.item_open_geo)
    TextView itemOpenGeo;
    @BindView(R.id.recycler_category)
    RecyclerView recyclerView;
    @BindView(R.id.item_drawer)
    FloatingActionButton buttonDraver;

    @BindView(R.id.layout_order)
    LinearLayout layoutOrder;
    @BindView(R.id.item_order_price)
    TextView textOrderPrice;
    @BindView(R.id.item_order_time)
    TextView textOrderTime;
    @BindView(R.id.item_order_status)
    TextView textOrderStatus;

    private MainViewModel viewModel;
    private UserLocationLayer userLocationLayer;
    ImageLoader imageLoader = ImageLoader.getInstance();

    private List<Company> companies;

    private DrivingRouter drivingRouter;
    private DrivingSession drivingSession;
    private MapObjectCollection mapObjects;
    private Point center;

    private CategoryCompanyAdapter categoryCompanyAdapter;

    private Map<Integer, List<Company>> mapCompany;
    private Order order;

    private PlacemarkMapObject selectMarker;
    private Polyline polylineMapObject;
    private PlacemarkMapObject myPosition;

    private IntentFilter filter;
    private BroadcastReceiver receiver;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map_yandex, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        MapKitFactory.setApiKey(MAPKIT_API_KEY);
        MapKitFactory.initialize(getActivity());
        DirectionsFactory.initialize(getActivity());
        mapCompany = new HashMap<>();
        super.onCreate(savedInstanceState);
    }

    //private Location location;

    private boolean allCompanyGet;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mapView.getMap().setRotateGesturesEnabled(false);
        mapView.getMap().move(new CameraPosition(MOSCOW, 10, 0, 0));
        viewModel = ViewModelProviders.of(this).get(MainViewModel.class);

        drivingRouter = DirectionsFactory.getInstance().createDrivingRouter();

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        categoryCompanyAdapter = new CategoryCompanyAdapter(getActivity(), new CategoryCompanyAdapter.CategoryCompanyListener() {
            @Override
            public void onCategoryCompany(final CategoryCompany item) {
                if (categoryCompanyAdapter.isChecked()) {

                    if (item.isCheck()) {
                        viewModel.getAllCompanyCategory(item.getId()).observeForever(new Observer<ObjectResponse>() {
                            @Override
                            public void onChanged(@Nullable ObjectResponse objectResponse) {
                                if (objectResponse.getObject() != null) {
                                    GetAllCompany allCompany = (GetAllCompany) objectResponse.getObject();
                                    if (item.isCheck()) {
                                        if (allCompanyGet) {
                                            allCompanyGet = false;
                                            Log.e("tr", "list replace");

                                            mapCompany.clear();
                                            mapCompany.put(item.getId(), allCompany.getCompanyList());
                                        } else {
                                            Log.e("tr", "add to list");
                                            mapCompany.put(item.getId(), allCompany.getCompanyList());
                                        }
                                    }

                                    addMarkers();
                                }
                            }
                        });
                    } else {
                        mapCompany.remove(item.getId());
                        addMarkers();
                    }

                } else {

                    getAllCompany();
                }

            }
        });
        recyclerView.setAdapter(categoryCompanyAdapter);

        userLocationLayer = mapView.getMap().getUserLocationLayer();
        userLocationLayer.setEnabled(true);
        userLocationLayer.setHeadingEnabled(true);
        userLocationLayer.setObjectListener(this);


        getOrders();

        filter = new IntentFilter("com.trablone.basket.service.status");
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int status = intent.getIntExtra("status", 0);
                textOrderStatus.setText(getOrderStatus(status));
                if (order != null){
                    order.setStatus(status);
                    if (order.getStatus() == 4){
                        viewModel.deleteOrder(order);
                        order = null;
                        mapCompany.clear();
                        polylineMapObject = null;
                        order = null;
                        layoutCompany.setVisibility(View.GONE);
                        layoutOrder.setVisibility(View.GONE);
                        getOrders();
                    }else {
                        viewModel.updateOrder(order);
                    }
                }
            }
        };

        getActivity().registerReceiver(receiver, filter);

    }

    private void getOrders() {
        viewModel.getOrders().observe(this, listResponse -> {
            if (listResponse.getList() != null && listResponse.getList().size() > 0) {
                List<Order> list = (List<Order>) listResponse.getList();


                order = list.get(0);

                viewModel.getOrderStatus(order.getOrder_id()).observe(CompanyYandexFragment.this, new Observer<ObjectResponse>() {
                    @Override
                    public void onChanged(@Nullable ObjectResponse objectResponse) {
                        GetOrderStatus orderStatus = (GetOrderStatus) objectResponse.getObject();
                        if (orderStatus.isStatus()) {
                            int status = orderStatus.getOrder_status();
                            textOrderStatus.setText(getOrderStatus(status));

                            if (status == 4){
                                viewModel.deleteOrder(order);
                                order = null;
                                mapCompany.clear();
                                polylineMapObject = null;
                                order = null;
                                layoutCompany.setVisibility(View.GONE);
                                layoutOrder.setVisibility(View.GONE);
                                getOrders();
                            }else {
                                order.setStatus(status);
                                //viewModel.updateOrder(order);
                                Point resultLocation = new Point(order.getCompany_x(), order.getCompany_y());

                                allCompanyGet = true;
                                mapCompany = new HashMap<>();
                                List<Company> companies = new ArrayList<>();

                                Company company = new Company();
                                company.setName(order.getCompany_name());
                                company.setCoord_x(order.getCompany_x());
                                company.setCoord_y(order.getCompany_y());
                                company.setId(order.getCompany_id());
                                company.setPicture(order.getCompany_image());
                                companies.add(company);
                                mapCompany.put(-1, companies);

                                addMarkers();

                                textOrderPrice.setText(String.valueOf(order.getSumma()) + "р");

                                if (myPosition != null) {

                                    try {
                                        double d = Geo.distance(myPosition.getGeometry(), resultLocation) / 1000;

                                        double t = d / 25;

                                        String[] time = String.format("%.2f", t).split(",");

                                        if (t < 1){
                                            textOrderTime.setText(String.valueOf(order.getTime_max()) + "м" + " / " + time[1] + "м");
                                        }else {
                                            textOrderTime.setText(String.valueOf(order.getTime_max()) + "м" + " / " + time[0] + "ч " + time[1] + "м");
                                        }
                                    }catch (Throwable e){
                                        e.printStackTrace();
                                    }
                                }

                                textOrderStatus.setText(getOrderStatus(order.getStatus()));

                                getCompanyView(company, resultLocation);

                                if (myPosition != null && myPosition.isValid()){
                                    submitRequest(resultLocation);
                                }
                            }
                        }
                    }
                });
            } else if (mapCompany.values().size() == 0){

                viewModel.getCategoryCompany().observeForever(new Observer<ObjectResponse>() {
                    @Override
                    public void onChanged(@Nullable ObjectResponse objectResponse) {
                        if (objectResponse.getObject() != null) {
                            GetCategoryCompany categoryCompany = (GetCategoryCompany) objectResponse.getObject();
                            categoryCompanyAdapter.setList(categoryCompany.getCategoryCompanyList());

                        }
                    }
                });

                getAllCompany();
            }
        });
    }

    @Override
    public void onObjectAdded(UserLocationView userLocationView) {
        myPosition = userLocationView.getArrow();

        if (order != null)
            viewModel.getGeo(myPosition.getGeometry().getLatitude(), myPosition.getGeometry().getLongitude()).observeForever(new Observer<ObjectResponse>() {
                @Override
                public void onChanged(@Nullable ObjectResponse objectResponse) {

                }
            });
        getOrders();
    }

    @Override
    public void onObjectRemoved(UserLocationView userLocationView) {

    }

    @Override
    public void onObjectUpdated(UserLocationView userLocationView, ObjectEvent event) {
        myPosition = userLocationView.getArrow();

        if (order != null)
            viewModel.getGeo(myPosition.getGeometry().getLatitude(), myPosition.getGeometry().getLongitude()).observeForever(new Observer<ObjectResponse>() {
                @Override
                public void onChanged(@Nullable ObjectResponse objectResponse) {

                }
            });
    }

    private void getAllCompany() {
        viewModel.getAllCompanies().observeForever(new Observer<ObjectResponse>() {
            @Override
            public void onChanged(@Nullable ObjectResponse objectResponse) {
                if (objectResponse.getObject() != null) {
                    allCompanyGet = true;
                    mapCompany.clear();
                    GetAllCompany allCompany = (GetAllCompany) objectResponse.getObject();
                    mapCompany.put(-1, allCompany.getCompanyList());

                    addMarkers();
                }
            }
        });
    }

    private void addMarkers() {
        Log.e("tr", "add markers");
        try {
            mapObjects = mapView.getMap().getMapObjects();

            mapObjects.clear();
            mapObjects.removeTapListener(tapListener);

            companies = new ArrayList<>();

            for (List<Company> list : mapCompany.values()) {
                companies.addAll(list);
            }

            if (polylineMapObject != null )
                mapObjects.addPolyline(polylineMapObject);

            if (getContext() != null){
                for (final Company company : companies) {
                    Point resultLocation = new Point(company.getCoord_x(), company.getCoord_y());
                    mapObjects.addPlacemark(resultLocation, ImageProvider.fromResource(getContext(), R.drawable.ic_location_on_black_24dp));
                }
            }


            mapObjects.addTapListener(tapListener);
        }catch (Throwable e){
            e.printStackTrace();
        }
    }

    private MapObjectTapListener tapListener = new MapObjectTapListener() {
        @Override
        public boolean onMapObjectTap(@NonNull MapObject mapObject, @NonNull Point point) {

            try {
                if (selectMarker != null && selectMarker.isValid()) {
                    ImageProvider imageProvider = ImageProvider.fromResource(getContext(), R.drawable.ic_location_on_black_24dp);
                    mapView.getMap().getMapObjects().remove(selectMarker);
                    mapView.getMap().getMapObjects().addPlacemark(selectMarker.getGeometry(), imageProvider);
                }
                PlacemarkMapObject item = (PlacemarkMapObject) mapObject;

                for (Company company : companies) {
                    Point point1 = item.getGeometry();

                    if (company.getCoord_x() == point1.getLatitude() && company.getCoord_y() == point1.getLongitude()) {
                        ImageProvider imageProvider = ImageProvider.fromResource(getContext(), R.drawable.ic_location_select);
                        mapView.getMap().getMapObjects().remove(mapObject);
                        selectMarker = mapView.getMap().getMapObjects().addPlacemark(item.getGeometry(), imageProvider);

                        getCompanyView(company, point1);
                    }
                }
            } catch (Throwable e) {
                e.printStackTrace();
            }

            return true;
        }
    };

    private void getCompanyView(final Company item, Point point) {

        layoutCompany.setVisibility(View.VISIBLE);

        imageLoader.displayImage(MainModule.BASE_URL + item.getPicture(), imageView);
        textView.setText(item.getName());
        textDesc.setText(item.getDescription());

        itemClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                layoutCompany.setVisibility(View.GONE);
                addMarkers();
            }
        });

        layoutCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CompanyActivity.showActivity(getContext(), item);
            }
        });
        textMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CompanyActivity.showActivity(getContext(), item);
            }
        });
        try {

            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:" + item.getCoord_x() + "," + item.getCoord_y()));
            itemOpenGeo.setVisibility(View.VISIBLE);
            itemOpenGeo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(intent);
                }
            });

        } catch (ActivityNotFoundException e) {
            itemOpenGeo.setVisibility(View.GONE);
        }
        if (order == null) {
            layoutOrder.setVisibility(View.GONE);
        }else {
            layoutOrder.setVisibility(View.VISIBLE);

        }
        if (myPosition != null) {

            Log.e("tr", " x " + myPosition.getGeometry().getLatitude() + " y " + myPosition.getGeometry().getLongitude());
            double d = Geo.distance(myPosition.getGeometry(), point) / 1000;
            int distance = (int) d;
            if (distance > 0) {
                textDistance.setText(distance + "км");
            } else {
                textDistance.setText(String.format("%.2fкм", d));
            }


        }
    }

    private String getOrderStatus(int status) {
        switch (status) {
            case 1:
                return "Новый";
            case 2:
                return "В работе";
            case 3:
                return "В доставке";
            case 4:
                return "Выполнен";
            default:
                return "Предварительный";
        }
    }

    @Override
    public void onDrivingRoutes(List<DrivingRoute> routes) {
        Log.e("tr", "routes " + routes.size());

        DrivingRoute route = routes.get(0);

        polylineMapObject = route.getGeometry();

        addMarkers();
    }

    @Override
    public void onDrivingRoutesError(@NonNull Error error) {
        Log.e("tr", "error " + error.isValid());

    }

    private void submitRequest(Point point) {

        Point start = myPosition.getGeometry();

        DrivingOptions options = new DrivingOptions();
        ArrayList<RequestPoint> requestPoints = new ArrayList<>();
        requestPoints.add(new RequestPoint(
                start,
                new ArrayList<Point>(),
                new ArrayList<DrivingArrivalPoint>(),
                RequestPointType.WAYPOINT));
        requestPoints.add(new RequestPoint(
                point,
                new ArrayList<Point>(),
                new ArrayList<DrivingArrivalPoint>(),
                RequestPointType.WAYPOINT));
        drivingSession = drivingRouter.requestRoutes(requestPoints, options, this);

        center = new Point(
                (start.getLatitude() + point.getLatitude()) / 2,
                (start.getLongitude() + point.getLongitude()) / 2);
    }


    /*@Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (locationHelper != null)
            locationHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (locationHelper != null)
            locationHelper.onActivityResult(requestCode, resultCode, data);
    }



    */

    @OnClick(R.id.item_drawer)
    public void toggle() {
        MainActivity activity = (MainActivity) getActivity();
        activity.toglle();
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
        MapKitFactory.getInstance().onStart();
    }

    @Override
    public void onStop() {
        mapView.onStop();
        MapKitFactory.getInstance().onStop();
        super.onStop();
    }
    @Override
    public void onDestroy() {
        if (getActivity() != null) {
            getActivity().unregisterReceiver(receiver);
        }
        super.onDestroy();
    }
}
