package com.trablone.basket.payment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.StringRes;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.wallet.AutoResolveHelper;
import com.google.android.gms.wallet.PaymentData;
import com.google.android.gms.wallet.PaymentDataRequest;
import com.google.android.gms.wallet.PaymentMethodToken;
import com.google.android.gms.wallet.PaymentsClient;
import com.google.android.gms.wallet.TransactionInfo;
import com.trablone.basket.R;
import com.trablone.basket.payment.api.Api;
import com.trablone.basket.payment.api.models.Transaction;
import com.trablone.basket.payment.api.response.ApiError;
import com.trablone.basket.payment.d3s.D3SDialog;
import com.trablone.basket.payment.d3s.D3SDialogListener;
import com.trablone.basket.payment.googlepay.Constants;
import com.trablone.basket.payment.googlepay.PaymentsUtil;

import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class PaymentHelper {
    private static final int LOAD_PAYMENT_DATA_REQUEST_CODE = 991;

    private PaymentsClient mPaymentsClient;
    private FragmentActivity context;
    private ProgressDialog dialog;

    public void setListener(CheckPayAvailable listener) {
        this.listener = listener;
    }

    private CheckPayAvailable listener;
    protected CompositeSubscription compositeSubscription = new CompositeSubscription();

    public void setPrice(int price) {
        this.price = price;
    }

    private int price;

    public String getMERCHANT_PUBLIC_ID() {
        return MERCHANT_PUBLIC_ID;
    }

    public void setMERCHANT_PUBLIC_ID(String MERCHANT_PUBLIC_ID) {
        this.MERCHANT_PUBLIC_ID = MERCHANT_PUBLIC_ID;
    }

    public String getMERCHANT_API_PASS() {
        return MERCHANT_API_PASS;
    }

    public void setMERCHANT_API_PASS(String MERCHANT_API_PASS) {
        this.MERCHANT_API_PASS = MERCHANT_API_PASS;
    }

    private String MERCHANT_PUBLIC_ID;
    private String MERCHANT_API_PASS;

    public PaymentHelper(FragmentActivity context) {
        this.context = context;
        mPaymentsClient = PaymentsUtil.createPaymentsClient(context);
        initLoadingDialog();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("tr", "result payment");
        switch (requestCode) {
            case LOAD_PAYMENT_DATA_REQUEST_CODE:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        PaymentData paymentData = PaymentData.getFromIntent(data);
                        handlePaymentSuccess(paymentData);
                        break;
                    case Activity.RESULT_CANCELED:
                        // Nothing to here normally - the user simply cancelled without selecting a
                        // payment method.
                        break;
                    case AutoResolveHelper.RESULT_ERROR:
                        Status status = AutoResolveHelper.getStatusFromIntent(data);
                        handleError(status.getStatusCode());
                        break;
                }

                break;
        }
    }

    private void show3DS(Transaction transaction) {

        D3SDialog.newInstance(context,
                transaction.getAcsUrl(),
                transaction.getId(),
                transaction.getPaReq(),
                Constants.TERM_URL,
                new D3SDialogListener() {
                    @Override
                    public void onAuthorizationCompleted(String md, String paRes) {
                        // Успешно
                        Log.e("tr", "md " + md + " pares " + paRes);
                        post3ds(md, paRes);
                    }

                    @Override
                    public void onAuthorizationFailed(int code, String message, String failedUrl) {
                        // Транзакция отклонена
                        showToast("AuthorizationFailed: " + message);
                    }
                }
        ).show();
    }

    public void showLoading() {
        if (dialog.isShowing()) {
            return;
        }

        dialog.show();
    }

    public void hideLoading() {
        if (!dialog.isShowing()) {
            return;
        }

        dialog.dismiss();
    }


    private void initLoadingDialog() {
        dialog = new ProgressDialog(context);
        dialog.setTitle(R.string.dialog_loading_title);
        dialog.setMessage(context.getResources().getString(R.string.dialog_loading_content));
        dialog.setCancelable(false);
    }

    public void charge(String cardCryptogramPacket, String cardHolderName, int amount) {
        compositeSubscription.add(Api
                .charge(cardCryptogramPacket, cardHolderName, amount, getMERCHANT_PUBLIC_ID(), getMERCHANT_API_PASS())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(this::showLoading)
                .doOnEach(notification -> hideLoading())
                .subscribe(this::checkResponse, this::handleError));
    }

    /*
    https://api.cloudpayments.ru/payments/cards/auth?CardCryptogramPacket=
    025375410635240904DEBzmg7ZhIWMGOmbGPeg8H3iHI6W1yTjlGyQBp6Gu4H8Hqe7K32/62/5f3HMuOiYFhrCn1EytMRrv5wv9N9Qcm09blZU8ZKkqoB0UUqUtF4bBCavZriGtMT9rt3LTZ/rIGUPVN/8dIjIU9b0pGnLDoW1MmdOL/83YsXyhPvyQT8LRcmwHrRCXnudOwy81k%2B7hC%2BVaY6Wjcdk2sajH5/LUzQmQwM3W0DMkS6ZdDThV/z0NrdyHyzEQh7eCBg52RZ9h5/OfP89cWm%2Bce%2B9Lmjf2r6ziV9yShxrq4T7S/FSOejn9IC1gHXEYxxcgC/gMEXEfdIm4RbIC6V1SKFAwQh%2Beg%3D%3D
    &Amount=1&Currency=RUB&IpAddress=192.168.0.1&Name=misha
     */
    public void auth(String cardCryptogramPacket, String cardHolderName, int amount) {

        compositeSubscription.add(Api
                .auth(cardCryptogramPacket, cardHolderName, amount, getMERCHANT_PUBLIC_ID(), getMERCHANT_API_PASS())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(this::showLoading)
                .doOnEach(notification -> hideLoading())
                .subscribe(this::checkResponse, this::handleError));
    }

    private void post3ds(String md, String paRes) {
        compositeSubscription.add(Api
                .post3ds(md, paRes, getMERCHANT_PUBLIC_ID(), getMERCHANT_API_PASS())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(this::showLoading)
                .doOnEach(notification -> hideLoading())
                .subscribe(this::checkResponse, this::handleError));
    }

    private void checkResponse (Transaction transaction) {
        if (transaction.getPaReq() != null && transaction.getAcsUrl() != null) {
            Log.e("tr", "show 3ds");
            show3DS(transaction);
        } else {
            showToast(transaction.getCardHolderMessage());
            listener.onCreateOrder();
        }
    }

    public void showToast(@StringRes int resId) {
        showToast(context.getString(resId));
    }

    public void showToast(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public void handleError(Throwable throwable, Class... ignoreClasses) {
        if (ignoreClasses.length > 0) {
            List<Class> classList = Arrays.asList(ignoreClasses);

            if (classList.contains(throwable.getClass())) {
                return;
            }
        }

        if (throwable instanceof ApiError) {
            ApiError apiError = (ApiError) throwable;

            String message = apiError.getMessage();
            showToast(message);
        } else if (throwable instanceof UnknownHostException) {
            showToast(R.string.common_no_internet_connection);
        } else {
            showToast(throwable.getMessage());
        }
    }

    private void handlePaymentSuccess(PaymentData paymentData) {

        PaymentMethodToken token = paymentData.getPaymentMethodToken();

        if (token != null) {
            String billingName = paymentData.getCardInfo().getBillingAddress().getName();
            Toast.makeText(context, context.getString(R.string.payments_show_name, billingName), Toast.LENGTH_LONG).show();

            Log.d("GooglePaymentToken", token.getToken());

            //if (payType == PAY_TYPE_CHARGE) {
            charge(token.getToken(), "Google Pay", price);
            //} else {
            //    auth(token.getToken(), "Google Pay");
            //}
        }
    }

    private void handleError(int statusCode) {

        Log.e("loadPaymentData failed", String.format("Error code: %d", statusCode));
    }



    public void requestPayment() {



        String price = PaymentsUtil.microsToString(this.price * 1000000);

        TransactionInfo transaction = PaymentsUtil.createTransaction(price);
        PaymentDataRequest request = PaymentsUtil.createPaymentDataRequest(transaction);
        Task<PaymentData> futurePaymentData = mPaymentsClient.loadPaymentData(request);

        AutoResolveHelper.resolveTask(futurePaymentData, context, LOAD_PAYMENT_DATA_REQUEST_CODE);
    }



    public void checkIsReadyToPay() {

        PaymentsUtil.isReadyToPay(mPaymentsClient).addOnCompleteListener(task -> {
            try {
                boolean result = task.getResult(ApiException.class);
                if (result){
                    listener.onSuccess();
                }else {
                    listener.onFailure();
                }
            } catch (ApiException exception) {
                listener.onFailure();
                Log.w("isReadyToPay failed", exception);
            }
        });
    }

    public interface CheckPayAvailable{
        void onSuccess();
        void onFailure();
        void onCreateOrder();
    }

    public void onDestroy() {
        compositeSubscription.unsubscribe();

    }
}
