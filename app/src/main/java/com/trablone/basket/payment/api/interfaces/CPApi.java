package com.trablone.basket.payment.api.interfaces;


import com.trablone.basket.payment.api.ApiMap;
import com.trablone.basket.payment.api.models.Transaction;
import com.trablone.basket.payment.api.response.ApiResponse;

import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

import rx.Observable;

public interface CPApi {

    @POST("payments/cards/charge")
    Observable<ApiResponse<Transaction>> charge(@Header("Content-Type") String contentType, @Header("Authorization") String authkey, @QueryMap ApiMap args);

    @POST("payments/cards/auth")
    Observable<ApiResponse<Transaction>> auth(@Header("Content-Type") String contentType, @Header("Authorization") String authkey, @QueryMap ApiMap args);

    @POST("payments/cards/post3ds")
    Observable<ApiResponse<Transaction>> post3ds(@Header("Content-Type") String contentType, @Header("Authorization") String authkey, @QueryMap ApiMap args);
}
