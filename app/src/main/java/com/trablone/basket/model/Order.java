package com.trablone.basket.model;


import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "order")
public class Order {

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public int getTime_max() {
        return time_max;
    }

    public void setTime_max(int time_max) {
        this.time_max = time_max;
    }

    public int getSumma() {
        return summa;
    }

    public void setSumma(int summa) {
        this.summa = summa;
    }


    private int order_id;
    private int time_max;
    private int summa;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    private int status;

    public int getCompany_id() {
        return company_id;
    }

    public void setCompany_id(int company_id) {
        this.company_id = company_id;
    }

    @PrimaryKey
    private int company_id;

    public double getCompany_x() {
        return company_x;
    }

    public void setCompany_x(double company_x) {
        this.company_x = company_x;
    }

    public double getCompany_y() {
        return company_y;
    }

    public void setCompany_y(double company_y) {
        this.company_y = company_y;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getCompany_image() {
        return company_image;
    }

    public void setCompany_image(String company_image) {
        this.company_image = company_image;
    }

    private double company_x;
    private double company_y;
    private String company_name;
    private String company_image;
}
