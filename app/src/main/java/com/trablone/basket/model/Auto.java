package com.trablone.basket.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "auto")
public class Auto implements Serializable {

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getAutoMark() {
        return autoMark;
    }

    public void setAutoMark(String autoMark) {
        this.autoMark = autoMark;
    }

    public String getAutoModel() {
        return autoModel;
    }

    public void setAutoModel(String autoModel) {
        this.autoModel = autoModel;
    }

    public String getAutoColor() {
        return autoColor;
    }

    public void setAutoColor(String autoColor) {
        this.autoColor = autoColor;
    }

    public int getAutoNumber() {
        return autoNumber;
    }

    public void setAutoNumber(int autoNumber) {
        this.autoNumber = autoNumber;
    }


    private int userId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @PrimaryKey
    private int id;
    private String autoMark;
    private String autoModel;
    private String autoColor;
    private int autoNumber;


    @Ignore
    public String getAuto(){
        StringBuilder builder = new StringBuilder();
        builder.append(autoMark);
        builder.append(" ");
        if (autoModel != null){
            builder.append(autoModel);
            builder.append(" ");
        }

        builder.append(autoColor);
        builder.append(" ");
        builder.append(autoNumber);
        return builder.toString();
    }
}
