package com.trablone.basket.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by trablone on 9/15/16.
 */
public class RouteResponce {

    public List<Route> routes;


    public Route getRoute() {
        if (routes != null && routes.size() > 0)
            return this.routes.get(0);
        return null;
    }

    public void setResponce(JSONObject responce) {
        try {
            Log.e("tr", "resp " + responce);
            JSONArray array = responce.getJSONArray("routes");
            routes = new ArrayList<>();
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                JSONObject overview = object.getJSONObject("overview_polyline");
                Route route = new Route();
                OverviewPolyline overviewPolyline = new OverviewPolyline();
                overviewPolyline.points = overview.getString("points");
                route.overview_polyline = overviewPolyline;

                JSONArray arrayLegs = object.getJSONArray("legs");
                for (int l = 0; l < arrayLegs.length(); l++) {
                    JSONObject leg = arrayLegs.getJSONObject(l);
                    JSONObject distance = leg.getJSONObject("distance");
                    route.distance = distance.getString("text");
                    JSONObject duration = leg.getJSONObject("duration");
                    route.duration = duration.getString("text");
                }

                routes.add(route);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public class Route {
        public OverviewPolyline overview_polyline;
        public String distance;
        public String duration;
    }

    public class OverviewPolyline {
        public String points;
    }
}
