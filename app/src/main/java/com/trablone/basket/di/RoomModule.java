package com.trablone.basket.di;

import android.app.Application;
import android.arch.persistence.room.Room;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


@Module
public class RoomModule {
    private final AppDatabase database;

    public RoomModule(Application application) {

        database = Room.databaseBuilder(application, AppDatabase.class, AppDatabase.DATABASE_NAME)
                .fallbackToDestructiveMigration().build();
    }

    @Provides
    @Singleton
    public AppDatabase providesAppDatabase() {
        return database;
    }

}
