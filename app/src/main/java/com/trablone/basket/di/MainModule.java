package com.trablone.basket.di;

import android.app.Application;
import android.content.Context;


import com.trablone.basket.rest.RetrofitApi;

import java.util.concurrent.Executors;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;


@Module
public class MainModule {

    //public static final String BASE_URL = "http://task5.litin.vn.ua/";
    public static final String BASE_URL = "http://basket.teo-crm.ru/";

    private final Application application;

    public MainModule(Application application) {
        this.application = application;
    }

    @Provides
    public Context providesAppContext() {
        return application;
    }

    @Provides
    @Singleton
    public RetrofitApi providesWebService() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .client(client)
                .build()
                .create(RetrofitApi.class);
    }

    @Provides
    @Singleton
    public AppExecutors providesAppExecutors() {
        return new AppExecutors(Executors.newSingleThreadExecutor(), Executors.newFixedThreadPool(3), new AppExecutors.MainThreadExecutor());
    }

    @Provides
    @Singleton
    public Repository providesRepository(AppDatabase appDatabase, RetrofitApi webService, AppExecutors executors) {
        return new Repository(appDatabase, webService, executors);
    }
}
