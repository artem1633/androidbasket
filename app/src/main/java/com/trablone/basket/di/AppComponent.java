package com.trablone.basket.di;


import android.content.Context;
import android.support.annotation.NonNull;


import com.trablone.basket.App;
import com.trablone.basket.OrderHistoryActivity;
import com.trablone.basket.viewModel.AboutViewModel;
import com.trablone.basket.viewModel.BasketViewModel;
import com.trablone.basket.viewModel.CompanyCategoryViewModel;
import com.trablone.basket.viewModel.MainViewModel;
import com.trablone.basket.viewModel.OrderHistoryViewModel;
import com.trablone.basket.viewModel.ProductViewModel;
import com.trablone.basket.viewModel.SupportViewModel;
import com.trablone.basket.viewModel.UserViewModel;

import javax.inject.Singleton;

import dagger.Component;

@Component (modules = {MainModule.class, RoomModule.class})
@Singleton
public abstract class AppComponent {

    public static AppComponent from(@NonNull Context context){
        return ((App) context.getApplicationContext()).getAppComponent();
    }

    public abstract void inject(App application);
    public abstract void inject(SupportViewModel application);
    public abstract void inject(AboutViewModel application);
    public abstract void inject(OrderHistoryViewModel application);
    public abstract void inject(MainViewModel viewModel);
    public abstract void inject(CompanyCategoryViewModel viewModel);
    public abstract void inject(ProductViewModel viewModel);
    public abstract void inject(BasketViewModel viewModel);
    public abstract void inject(UserViewModel viewModel);

}
