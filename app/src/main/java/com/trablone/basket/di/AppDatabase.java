package com.trablone.basket.di;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.widget.ProgressBar;

import com.trablone.basket.dao.AutoDao;
import com.trablone.basket.dao.CardDao;
import com.trablone.basket.dao.OrderDao;
import com.trablone.basket.dao.ProductDao;
import com.trablone.basket.dao.UserDao;
import com.trablone.basket.model.Auto;
import com.trablone.basket.model.Card;
import com.trablone.basket.model.Company;
import com.trablone.basket.model.Order;
import com.trablone.basket.model.Product;
import com.trablone.basket.model.User;


@Database(entities = {Product.class, User.class, Auto.class, Card.class, Order.class}, version = 12,  exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    private static final String LOG_TAG = AppDatabase.class.getSimpleName();
    public static final String DATABASE_NAME = "database";

    public abstract ProductDao getProductDao();
    public abstract UserDao getUserDao();
    public abstract AutoDao getAutoDao();
    public abstract CardDao getCardDao();
    public abstract OrderDao getOrderDao();
}
