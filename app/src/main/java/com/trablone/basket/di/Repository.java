package com.trablone.basket.di;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;
import android.util.Log;

import com.trablone.basket.App;
import com.trablone.basket.model.Auto;
import com.trablone.basket.model.Card;
import com.trablone.basket.model.Order;
import com.trablone.basket.model.Product;
import com.trablone.basket.model.User;
import com.trablone.basket.response.ListResponse;
import com.trablone.basket.response.ObjectResponse;
import com.trablone.basket.rest.GetAbout;
import com.trablone.basket.rest.GetAllCompany;
import com.trablone.basket.rest.GetCategoryCompany;
import com.trablone.basket.rest.GetCategoryProduct;
import com.trablone.basket.rest.GetConfirmRegister;
import com.trablone.basket.rest.GetCreateOrder;
import com.trablone.basket.rest.GetGeo;
import com.trablone.basket.rest.GetMerchant;
import com.trablone.basket.rest.GetOrderHistory;
import com.trablone.basket.rest.GetOrderStatus;
import com.trablone.basket.rest.GetProduct;
import com.trablone.basket.rest.GetRegister;
import com.trablone.basket.rest.GetSupport;
import com.trablone.basket.rest.RetrofitApi;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.QueryMap;


public class Repository {

    private final AppDatabase appDatabase;
    private final RetrofitApi webService;
    private AppExecutors executors;

    public void setLocale(String locale) {
        this.locale = locale;
    }

    private String locale;

    public Repository(AppDatabase appDatabase, RetrofitApi wService, AppExecutors executors) {
        this.appDatabase = appDatabase;
        this.webService = wService;
        this.executors = executors;
    }

    public LiveData<ObjectResponse> createOrder(int company, String order, @QueryMap HashMap<String, String> map) {
        final MutableLiveData<ObjectResponse> mediatorLiveData = new MutableLiveData<>();

        getRemoteCreateOrder(company, order, map).observeForever(new Observer<ObjectResponse>() {
            @Override
            public void onChanged(@Nullable ObjectResponse listResponse) {
                mediatorLiveData.postValue(listResponse);
            }
        });
        return mediatorLiveData;
    }


    private LiveData<ObjectResponse> getRemoteCreateOrder(int company, String order, @QueryMap HashMap<String, String> map) {
        final MutableLiveData<ObjectResponse> locationResponse = new MutableLiveData<>();
        webService.createOrder(App.API_KEY, company, order, map).enqueue(new Callback<GetCreateOrder>() {
            @Override
            public void onResponse(Call<GetCreateOrder> call, Response<GetCreateOrder> response) {
                Log.e("tr", "response: " + response.isSuccessful());
                if (response.isSuccessful()) {
                    final GetCreateOrder locations = response.body();
                    if (locations != null) {

                        locationResponse.postValue(new ObjectResponse(locations));

                    } else {
                        locationResponse.postValue(new ObjectResponse(null));
                    }


                }
            }

            @Override
            public void onFailure(Call<GetCreateOrder> call, Throwable t) {
                Log.e("tr", "response: " + t.getMessage());
                locationResponse.postValue(new ObjectResponse(null));
            }
        });
        return locationResponse;
    }

    public LiveData<ObjectResponse> register(String name, String phone) {
        final MutableLiveData<ObjectResponse> mediatorLiveData = new MutableLiveData<>();

        getRemoteRegister(name, phone).observeForever(new Observer<ObjectResponse>() {
            @Override
            public void onChanged(@Nullable ObjectResponse listResponse) {
                mediatorLiveData.postValue(listResponse);
            }
        });
        return mediatorLiveData;
    }


    private LiveData<ObjectResponse> getRemoteRegister(String name, String phone) {
        final MutableLiveData<ObjectResponse> locationResponse = new MutableLiveData<>();
        webService.register(name, phone).enqueue(new Callback<GetRegister>() {
            @Override
            public void onResponse(Call<GetRegister> call, Response<GetRegister> response) {
                Log.e("tr", "response: " + response.isSuccessful());
                if (response.isSuccessful()) {
                    final GetRegister locations = response.body();
                    if (locations != null) {

                        locationResponse.postValue(new ObjectResponse(locations));

                    } else {
                        locationResponse.postValue(new ObjectResponse(null));
                    }


                }
            }

            @Override
            public void onFailure(Call<GetRegister> call, Throwable t) {
                Log.e("tr", "response: " + t.getMessage());
                locationResponse.postValue(new ObjectResponse(null));
            }
        });
        return locationResponse;
    }

    public LiveData<ObjectResponse> confirmRegister(String sms, String phone, String token) {
        final MutableLiveData<ObjectResponse> mediatorLiveData = new MutableLiveData<>();

        getRemoteConfirmRegister(sms, phone, token).observeForever(new Observer<ObjectResponse>() {
            @Override
            public void onChanged(@Nullable ObjectResponse listResponse) {
                mediatorLiveData.postValue(listResponse);
            }
        });
        return mediatorLiveData;
    }

    public LiveData<ObjectResponse> confirmRegister(String phone) {
        final MutableLiveData<ObjectResponse> mediatorLiveData = new MutableLiveData<>();

        getRemoteConfirmRegister(phone).observeForever(new Observer<ObjectResponse>() {
            @Override
            public void onChanged(@Nullable ObjectResponse listResponse) {
                mediatorLiveData.postValue(listResponse);
            }
        });
        return mediatorLiveData;
    }


    private LiveData<ObjectResponse> getRemoteConfirmRegister(String sms, String phone, String token) {
        final MutableLiveData<ObjectResponse> locationResponse = new MutableLiveData<>();
        webService.confirmRegister(sms, phone, token).enqueue(new Callback<GetConfirmRegister>() {
            @Override
            public void onResponse(Call<GetConfirmRegister> call, Response<GetConfirmRegister> response) {
                Log.e("tr", "response: " + response.isSuccessful());
                if (response.isSuccessful()) {
                    final GetConfirmRegister locations = response.body();
                    if (locations != null) {

                        locationResponse.postValue(new ObjectResponse(locations));

                    } else {
                        locationResponse.postValue(new ObjectResponse(null));
                    }


                }
            }

            @Override
            public void onFailure(Call<GetConfirmRegister> call, Throwable t) {
                Log.e("tr", "response: " + t.getMessage());
                locationResponse.postValue(new ObjectResponse(null));
            }
        });
        return locationResponse;
    }

    private LiveData<ObjectResponse> getRemoteConfirmRegister(String phone) {
        final MutableLiveData<ObjectResponse> locationResponse = new MutableLiveData<>();
        webService.confirmRegister(phone).enqueue(new Callback<GetConfirmRegister>() {
            @Override
            public void onResponse(Call<GetConfirmRegister> call, Response<GetConfirmRegister> response) {
                Log.e("tr", "response: " + response.isSuccessful());
                if (response.isSuccessful()) {
                    final GetConfirmRegister locations = response.body();
                    if (locations != null) {

                        locationResponse.postValue(new ObjectResponse(locations));

                    } else {
                        locationResponse.postValue(new ObjectResponse(null));
                    }


                }
            }

            @Override
            public void onFailure(Call<GetConfirmRegister> call, Throwable t) {
                Log.e("tr", "response: " + t.getMessage());
                locationResponse.postValue(new ObjectResponse(null));
            }
        });
        return locationResponse;
    }


    public LiveData<ObjectResponse> getOrderStatus(int order_id) {
        final MutableLiveData<ObjectResponse> mediatorLiveData = new MutableLiveData<>();

        getRemoteOrderStatus(order_id).observeForever(new Observer<ObjectResponse>() {
            @Override
            public void onChanged(@Nullable ObjectResponse listResponse) {
                mediatorLiveData.postValue(listResponse);
            }
        });
        return mediatorLiveData;
    }


    private LiveData<ObjectResponse> getRemoteOrderStatus(int order_id) {
        final MutableLiveData<ObjectResponse> locationResponse = new MutableLiveData<>();
        webService.getOrderStatus(App.API_KEY, order_id).enqueue(new Callback<GetOrderStatus>() {
            @Override
            public void onResponse(Call<GetOrderStatus> call, Response<GetOrderStatus> response) {
                Log.e("tr", "response: " + response.isSuccessful());
                if (response.isSuccessful()) {
                    final GetOrderStatus locations = response.body();
                    if (locations != null) {

                        locationResponse.postValue(new ObjectResponse(locations));

                    } else {
                        locationResponse.postValue(new ObjectResponse(null));
                    }


                }
            }

            @Override
            public void onFailure(Call<GetOrderStatus> call, Throwable t) {
                Log.e("tr", "response: " + t.getMessage());
                locationResponse.postValue(new ObjectResponse(null));
            }
        });
        return locationResponse;
    }

    public LiveData<ObjectResponse> getGeo(double x, double y) {
        final MutableLiveData<ObjectResponse> mediatorLiveData = new MutableLiveData<>();

        getRemoteGeo(x, y).observeForever(new Observer<ObjectResponse>() {
            @Override
            public void onChanged(@Nullable ObjectResponse listResponse) {
                mediatorLiveData.postValue(listResponse);
            }
        });
        return mediatorLiveData;
    }


    private LiveData<ObjectResponse> getRemoteGeo(double x, double y) {
        final MutableLiveData<ObjectResponse> locationResponse = new MutableLiveData<>();
        webService.getGeo(App.API_KEY, String.valueOf(x), String.valueOf(y)).enqueue(new Callback<GetGeo>() {
            @Override
            public void onResponse(Call<GetGeo> call, Response<GetGeo> response) {
                Log.e("tr", "response: " + response.isSuccessful());
                if (response.isSuccessful()) {
                    final GetGeo locations = response.body();
                    if (locations != null) {

                        locationResponse.postValue(new ObjectResponse(locations));

                    } else {
                        locationResponse.postValue(new ObjectResponse(null));
                    }


                }
            }

            @Override
            public void onFailure(Call<GetGeo> call, Throwable t) {
                Log.e("tr", "response: " + t.getMessage());
                locationResponse.postValue(new ObjectResponse(null));
            }
        });
        return locationResponse;
    }

    public LiveData<ObjectResponse> getMerchant() {
        final MutableLiveData<ObjectResponse> mediatorLiveData = new MutableLiveData<>();

        getRemoteMershant().observeForever(new Observer<ObjectResponse>() {
            @Override
            public void onChanged(@Nullable ObjectResponse listResponse) {
                mediatorLiveData.postValue(listResponse);
            }
        });
        return mediatorLiveData;
    }


    private LiveData<ObjectResponse> getRemoteMershant() {
        final MutableLiveData<ObjectResponse> locationResponse = new MutableLiveData<>();
        webService.getMerchant(App.API_KEY).enqueue(new Callback<GetMerchant>() {
            @Override
            public void onResponse(Call<GetMerchant> call, Response<GetMerchant> response) {
                Log.e("tr", "response: " + response.isSuccessful());
                if (response.isSuccessful()) {
                    final GetMerchant locations = response.body();
                    if (locations != null) {

                        locationResponse.postValue(new ObjectResponse(locations));

                    } else {
                        locationResponse.postValue(new ObjectResponse(null));
                    }


                }
            }

            @Override
            public void onFailure(Call<GetMerchant> call, Throwable t) {
                Log.e("tr", "response: " + t.getMessage());
                locationResponse.postValue(new ObjectResponse(null));
            }
        });
        return locationResponse;
    }

    public void updateProduct(final Product account) {
        executors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                appDatabase.getProductDao().update(account);
            }
        });

    }

    public void insertAuto(final Auto account) {
        executors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                appDatabase.getAutoDao().bulkInsert(account);
            }
        });

    }

    public void updateOrder(final Order account) {
        executors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                appDatabase.getOrderDao().update(account);
            }
        });

    }

    public void insertOrder(final Order account) {
        executors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                appDatabase.getOrderDao().bulkInsert(account);
            }
        });

    }

    public void insertCard(final Card account) {
        executors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                appDatabase.getCardDao().bulkInsert(account);
            }
        });

    }

    public void deleteAuto(final Auto account) {
        executors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                appDatabase.getAutoDao().delete(account);
            }
        });

    }

    public void deleteCard(final Card account) {
        executors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                appDatabase.getCardDao().delete(account);
            }
        });

    }

    public void deleteOrder(final Order account) {
        if (account != null)
        executors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                appDatabase.getOrderDao().delete(account);
            }
        });

    }

    public void updateAuto(final Auto account) {
        executors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                appDatabase.getAutoDao().update(account);
            }
        });

    }

    public void insertUser(final User account) {
        executors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                appDatabase.getUserDao().bulkInsert(account);
            }
        });

    }

    public void deleteUser() {
        executors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                appDatabase.getUserDao().delete();
                appDatabase.getOrderDao().delete();
                appDatabase.getAutoDao().delete();
                appDatabase.getProductDao().delete();
                appDatabase.getCardDao().delete();
            }
        });

    }

    public LiveData<ListResponse> getProductBasket(final int company) {
        final MediatorLiveData<ListResponse> mediatorLiveData = new MediatorLiveData<>();
        executors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                final LiveData<List<Product>> localPosts = appDatabase.getProductDao().getProductsCompanyCategoryBasket(company);
                mediatorLiveData.addSource(localPosts, new Observer<List<Product>>() {
                    @Override
                    public void onChanged(@Nullable List<Product> accounts) {
                        mediatorLiveData.postValue(new ListResponse(accounts));
                    }
                });
            }
        });

        return mediatorLiveData;
    }

    public LiveData<ListResponse> getAutos() {
        final MediatorLiveData<ListResponse> mediatorLiveData = new MediatorLiveData<>();
        executors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                final LiveData<List<Auto>> localPosts = appDatabase.getAutoDao().getAutos();
                mediatorLiveData.addSource(localPosts, new Observer<List<Auto>>() {
                    @Override
                    public void onChanged(@Nullable List<Auto> accounts) {
                        mediatorLiveData.postValue(new ListResponse(accounts));
                    }
                });
            }
        });

        return mediatorLiveData;
    }

    public LiveData<ListResponse> getCards() {
        final MediatorLiveData<ListResponse> mediatorLiveData = new MediatorLiveData<>();
        executors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                final LiveData<List<Card>> localPosts = appDatabase.getCardDao().getCards();
                mediatorLiveData.addSource(localPosts, new Observer<List<Card>>() {
                    @Override
                    public void onChanged(@Nullable List<Card> accounts) {
                        mediatorLiveData.postValue(new ListResponse(accounts));
                    }
                });
            }
        });

        return mediatorLiveData;
    }

    public LiveData<ObjectResponse> getUser() {
        final MediatorLiveData<ObjectResponse> mediatorLiveData = new MediatorLiveData<>();
        executors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                final LiveData<User> localPosts = appDatabase.getUserDao().getUser();
                mediatorLiveData.addSource(localPosts, new Observer<User>() {
                    @Override
                    public void onChanged(@Nullable User accounts) {
                        mediatorLiveData.postValue(new ObjectResponse(accounts));
                    }
                });
            }
        });

        return mediatorLiveData;
    }

    public LiveData<ListResponse> getOrders() {
        final MediatorLiveData<ListResponse> mediatorLiveData = new MediatorLiveData<>();
        executors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                final LiveData<List<Order>> localPosts = appDatabase.getOrderDao().getOrders();
                mediatorLiveData.addSource(localPosts, new Observer<List<Order>>() {
                    @Override
                    public void onChanged(@Nullable List<Order> accounts) {
                        mediatorLiveData.postValue(new ListResponse(accounts));
                    }
                });
            }
        });

        return mediatorLiveData;
    }


    public LiveData<ObjectResponse> getProducts(final int cat, final int company) {
        final MutableLiveData<ObjectResponse> mediatorLiveData = new MutableLiveData<>();
        getRemoteProducts(cat, company).observeForever(new Observer<ObjectResponse>() {
            @Override
            public void onChanged(@Nullable ObjectResponse objectResponse) {
                mediatorLiveData.postValue(objectResponse);
            }
        });

        return mediatorLiveData;
    }

    private LiveData<ObjectResponse> getRemoteProducts(int cat, int company) {
        final MutableLiveData<ObjectResponse> locationResponse = new MutableLiveData<>();
        webService.getProductCategory(App.API_KEY, cat, company).enqueue(new Callback<GetProduct>() {
            @Override
            public void onResponse(Call<GetProduct> call, Response<GetProduct> response) {
                Log.e("tr", "response: " + response.isSuccessful());
                if (response.isSuccessful()) {
                    final GetProduct locations = response.body();
                    if (locations != null && locations.isStatus()) {

                        List<Product> products = locations.getProductList();

                        executors.diskIO().execute(new Runnable() {
                            @Override
                            public void run() {
                                for (Product item : products) {
                                    //Log.e("tr", "item " + item.getName() + " " + item.getCount() + " " + item.getId());

                                    Product product = appDatabase.getProductDao().getProduct(item.getId());
                                    if (product != null) {
                                        //Log.e("tr", "product " + product.getName() + " " + product.getCount() + " " +  product.getId());
                                        if (item.getCount() < product.getCount()) {

                                            item.setCategory(product.getCategory());
                                            item.setCompany(product.getCompany());
                                            item.setCount(product.getCount());
                                            //Log.e("tr", "update product " + item.getName() + " " + item.getCount());
                                        }
                                    }
                                }
                                appDatabase.getProductDao().bulkInsert(products);
                                locationResponse.postValue(new ObjectResponse(locations));
                                //Log.e("tr", "insert products");
                            }
                        });

                    } else {
                        locationResponse.postValue(new ObjectResponse(null));
                    }
                }
            }

            @Override
            public void onFailure(Call<GetProduct> call, Throwable t) {
                Log.e("tr", "response: " + t.getMessage());
                locationResponse.postValue(new ObjectResponse(null));
            }
        });
        return locationResponse;
    }

    public LiveData<ObjectResponse> getAllCompany() {
        final MutableLiveData<ObjectResponse> mediatorLiveData = new MutableLiveData<>();

        getRemoteAllCompany().observeForever(new Observer<ObjectResponse>() {
            @Override
            public void onChanged(@Nullable ObjectResponse listResponse) {
                mediatorLiveData.postValue(listResponse);
            }
        });
        return mediatorLiveData;
    }


    private LiveData<ObjectResponse> getRemoteAllCompany() {
        final MutableLiveData<ObjectResponse> locationResponse = new MutableLiveData<>();
        webService.getAllCompany(App.API_KEY).enqueue(new Callback<GetAllCompany>() {
            @Override
            public void onResponse(Call<GetAllCompany> call, Response<GetAllCompany> response) {
                Log.e("tr", "response: " + response.isSuccessful());
                if (response.isSuccessful()) {
                    final GetAllCompany locations = response.body();
                    if (locations != null) {

                        locationResponse.postValue(new ObjectResponse(locations));

                    } else {
                        locationResponse.postValue(new ObjectResponse(null));
                    }


                }
            }

            @Override
            public void onFailure(Call<GetAllCompany> call, Throwable t) {
                Log.e("tr", "response: " + t.getMessage());
                locationResponse.postValue(new ObjectResponse(null));
            }
        });
        return locationResponse;
    }

    public LiveData<ObjectResponse> getCategoryProductCompany(int cat) {
        final MutableLiveData<ObjectResponse> mediatorLiveData = new MutableLiveData<>();

        getRemoteCategoryProductCompany(cat).observeForever(new Observer<ObjectResponse>() {
            @Override
            public void onChanged(@Nullable ObjectResponse listResponse) {
                mediatorLiveData.postValue(listResponse);
            }
        });
        return mediatorLiveData;
    }

    private LiveData<ObjectResponse> getRemoteCategoryProductCompany(int cat) {
        final MutableLiveData<ObjectResponse> locationResponse = new MutableLiveData<>();
        webService.getCategoryProductCompany(App.API_KEY, cat).enqueue(new Callback<GetCategoryProduct>() {
            @Override
            public void onResponse(Call<GetCategoryProduct> call, Response<GetCategoryProduct> response) {
                Log.e("tr", "response: " + response.isSuccessful());
                if (response.isSuccessful()) {
                    final GetCategoryProduct locations = response.body();
                    if (locations != null) {

                        locationResponse.postValue(new ObjectResponse(locations));

                    } else {
                        locationResponse.postValue(new ObjectResponse(null));
                    }


                }
            }

            @Override
            public void onFailure(Call<GetCategoryProduct> call, Throwable t) {
                Log.e("tr", "response: " + t.getMessage());
                locationResponse.postValue(new ObjectResponse(null));
            }
        });
        return locationResponse;
    }

    public LiveData<ObjectResponse> getAllCompanyCategory(int cat) {
        final MutableLiveData<ObjectResponse> mediatorLiveData = new MutableLiveData<>();

        getRemoteAllCompanyCategory(cat).observeForever(new Observer<ObjectResponse>() {
            @Override
            public void onChanged(@Nullable ObjectResponse listResponse) {
                mediatorLiveData.postValue(listResponse);
            }
        });
        return mediatorLiveData;
    }


    private LiveData<ObjectResponse> getRemoteAllCompanyCategory(int cat) {
        final MutableLiveData<ObjectResponse> locationResponse = new MutableLiveData<>();
        webService.getAllCompanyCategory(App.API_KEY, cat).enqueue(new Callback<GetAllCompany>() {
            @Override
            public void onResponse(Call<GetAllCompany> call, Response<GetAllCompany> response) {
                Log.e("tr", "response: " + response.isSuccessful());
                if (response.isSuccessful()) {
                    final GetAllCompany locations = response.body();
                    if (locations != null) {

                        locationResponse.postValue(new ObjectResponse(locations));

                    } else {
                        locationResponse.postValue(new ObjectResponse(null));
                    }


                }
            }

            @Override
            public void onFailure(Call<GetAllCompany> call, Throwable t) {
                Log.e("tr", "response: " + t.getMessage());
                locationResponse.postValue(new ObjectResponse(null));
            }
        });
        return locationResponse;
    }

    public LiveData<ObjectResponse> getOrderHistory(int limit, int offset) {
        final MutableLiveData<ObjectResponse> mediatorLiveData = new MutableLiveData<>();

        getRemoteOrderHistory(limit, offset).observeForever(new Observer<ObjectResponse>() {
            @Override
            public void onChanged(@Nullable ObjectResponse listResponse) {
                mediatorLiveData.postValue(listResponse);
            }
        });
        return mediatorLiveData;
    }


    private LiveData<ObjectResponse> getRemoteOrderHistory(int limit, int offset) {
        final MutableLiveData<ObjectResponse> locationResponse = new MutableLiveData<>();
        webService.getOrderHistory(App.API_KEY, offset, limit).enqueue(new Callback<GetOrderHistory>() {
            @Override
            public void onResponse(Call<GetOrderHistory> call, Response<GetOrderHistory> response) {
                Log.e("tr", "response: " + response.isSuccessful());
                if (response.isSuccessful()) {
                    final GetOrderHistory locations = response.body();
                    if (locations != null) {

                        locationResponse.postValue(new ObjectResponse(locations));

                    } else {
                        locationResponse.postValue(new ObjectResponse(null));
                    }


                }
            }

            @Override
            public void onFailure(Call<GetOrderHistory> call, Throwable t) {
                Log.e("tr", "response: " + t.getMessage());
                locationResponse.postValue(new ObjectResponse(null));
            }
        });
        return locationResponse;
    }

    public LiveData<ObjectResponse> getSupport() {
        final MutableLiveData<ObjectResponse> mediatorLiveData = new MutableLiveData<>();

        getRemoteSupport().observeForever(new Observer<ObjectResponse>() {
            @Override
            public void onChanged(@Nullable ObjectResponse listResponse) {
                mediatorLiveData.postValue(listResponse);
            }
        });
        return mediatorLiveData;
    }


    private LiveData<ObjectResponse> getRemoteSupport() {
        final MutableLiveData<ObjectResponse> locationResponse = new MutableLiveData<>();
        webService.getSupport(App.API_KEY).enqueue(new Callback<GetSupport>() {
            @Override
            public void onResponse(Call<GetSupport> call, Response<GetSupport> response) {
                Log.e("tr", "response: " + response.isSuccessful());
                if (response.isSuccessful()) {
                    final GetSupport locations = response.body();
                    if (locations != null) {

                        locationResponse.postValue(new ObjectResponse(locations));

                    } else {
                        locationResponse.postValue(new ObjectResponse(null));
                    }


                }
            }

            @Override
            public void onFailure(Call<GetSupport> call, Throwable t) {
                Log.e("tr", "response: " + t.getMessage());
                locationResponse.postValue(new ObjectResponse(null));
            }
        });
        return locationResponse;
    }

    public LiveData<ObjectResponse> getAbout() {
        final MutableLiveData<ObjectResponse> mediatorLiveData = new MutableLiveData<>();

        getRemoteAbout().observeForever(new Observer<ObjectResponse>() {
            @Override
            public void onChanged(@Nullable ObjectResponse listResponse) {
                mediatorLiveData.postValue(listResponse);
            }
        });
        return mediatorLiveData;
    }


    private LiveData<ObjectResponse> getRemoteAbout() {
        final MutableLiveData<ObjectResponse> locationResponse = new MutableLiveData<>();
        webService.getAbout(App.API_KEY).enqueue(new Callback<GetAbout>() {
            @Override
            public void onResponse(Call<GetAbout> call, Response<GetAbout> response) {
                Log.e("tr", "response: " + response.isSuccessful());
                if (response.isSuccessful()) {
                    final GetAbout locations = response.body();
                    if (locations != null) {
                        locationResponse.postValue(new ObjectResponse(locations));

                    } else {
                        locationResponse.postValue(new ObjectResponse(null));
                    }
                }
            }

            @Override
            public void onFailure(Call<GetAbout> call, Throwable t) {
                Log.e("tr", "response: " + t.getMessage());
                locationResponse.postValue(new ObjectResponse(null));
            }
        });
        return locationResponse;
    }

    public LiveData<ObjectResponse> getCategoryCompany() {
        final MutableLiveData<ObjectResponse> mediatorLiveData = new MutableLiveData<>();

        getRemoteCategoryCompany().observeForever(new Observer<ObjectResponse>() {
            @Override
            public void onChanged(@Nullable ObjectResponse listResponse) {
                mediatorLiveData.postValue(listResponse);
            }
        });
        return mediatorLiveData;
    }


    private LiveData<ObjectResponse> getRemoteCategoryCompany() {
        final MutableLiveData<ObjectResponse> locationResponse = new MutableLiveData<>();
        webService.getCategoryCompany(App.API_KEY).enqueue(new Callback<GetCategoryCompany>() {
            @Override
            public void onResponse(Call<GetCategoryCompany> call, Response<GetCategoryCompany> response) {
                Log.e("tr", "response: " + response.isSuccessful());
                if (response.isSuccessful()) {
                    final GetCategoryCompany locations = response.body();
                    if (locations != null) {

                        locationResponse.postValue(new ObjectResponse(locations));

                    } else {
                        locationResponse.postValue(new ObjectResponse(null));
                    }


                }
            }

            @Override
            public void onFailure(Call<GetCategoryCompany> call, Throwable t) {
                Log.e("tr", "response: " + t.getMessage());
                locationResponse.postValue(new ObjectResponse(null));
            }
        });
        return locationResponse;
    }

    public LiveData<ObjectResponse> getRoutes(HashMap<String, String> map) {
        final MutableLiveData<ObjectResponse> mediatorLiveData = new MutableLiveData<>();

        getRemoteRoutes(map).observeForever(new Observer<ObjectResponse>() {
            @Override
            public void onChanged(@Nullable ObjectResponse listResponse) {
                mediatorLiveData.postValue(listResponse);
            }
        });
        return mediatorLiveData;
    }


    private LiveData<ObjectResponse> getRemoteRoutes(HashMap<String, String> map) {
        final MutableLiveData<ObjectResponse> locationResponse = new MutableLiveData<>();
        webService.getRoutes("https://maps.googleapis.com/maps/api/directions/json", map).enqueue(new Callback<JSONObject>() {
            @Override
            public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                Log.e("tr", "response: " + response.isSuccessful());
                if (response.isSuccessful()) {
                    final JSONObject locations = response.body();
                    Log.e("tr", "response: " + locations);
                    if (locations != null) {
                        locationResponse.postValue(new ObjectResponse(locations));

                    } else {
                        locationResponse.postValue(new ObjectResponse(null));
                    }


                }
            }

            @Override
            public void onFailure(Call<JSONObject> call, Throwable t) {
                Log.e("tr", "response: " + t.getMessage());
                locationResponse.postValue(new ObjectResponse(null));
            }
        });
        return locationResponse;
    }


    public LiveData<ResponseBody> getDownloadFile(String path) {
        final MutableLiveData<ResponseBody> mediatorLiveData = new MutableLiveData<>();

        getRemoteDownloadFile(path).observeForever(new Observer<ResponseBody>() {
            @Override
            public void onChanged(@Nullable ResponseBody listResponse) {
                mediatorLiveData.postValue(listResponse);
            }
        });
        return mediatorLiveData;
    }


    private LiveData<ResponseBody> getRemoteDownloadFile(String patch) {
        final MutableLiveData<ResponseBody> locationResponse = new MutableLiveData<>();
        webService.downloadFile(patch).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.e("tr", "response: " + response.isSuccessful());
                if (response.isSuccessful()) {
                    final ResponseBody locations = response.body();
                    if (locations != null) {
                        locationResponse.postValue(locations);

                    } else {
                        locationResponse.postValue(null);
                    }


                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("tr", "response: " + t.getMessage());
                locationResponse.postValue(null);
            }
        });
        return locationResponse;
    }


}
