package com.trablone.basket;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.trablone.basket.fragment.AutoFragment;
import com.trablone.basket.fragment.PaymentFragment;
import com.trablone.basket.model.Auto;
import com.trablone.basket.model.Company;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ActivityConfirmOrder extends AppCompatActivity {

    public static void showActivity(Context context, int price, Company company){
        Intent intent = new Intent(context, ActivityConfirmOrder.class);
        intent.putExtra("price", price);
        intent.putExtra("company", company);
        context.startActivity(intent);
    }

    @BindView(R.id.toolbar)
    Toolbar toolbar;


    private ActionBar actionBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_order);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        actionBar.setTitle("Выбор авто");
        showFragment(new AutoFragment());

    }

    private void showFragment(Fragment fragment){
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, fragment)
                .commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }


    public void next(Auto auto){
        actionBar.setTitle("Выбор способа оплаты");
        showFragment(PaymentFragment.getInstance(auto));
    }

}
