package com.trablone.basket;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;


import com.google.firebase.FirebaseApp;
import com.google.firebase.messaging.FirebaseMessaging;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.trablone.basket.di.AppComponent;
import com.trablone.basket.di.DaggerAppComponent;
import com.trablone.basket.di.MainModule;
import com.trablone.basket.di.RoomModule;
import com.yandex.metrica.YandexMetrica;
import com.yandex.metrica.YandexMetricaConfig;


public class App extends MultiDexApplication {


    public static void setApiKey(String apiKey) {
        API_KEY = apiKey;
    }

    public static String API_KEY = "b9EqBtQJSQkFNdeJ8K4uicIOh2EXuoir";

    public AppComponent getAppComponent() {
        return appComponent;
    }

    private AppComponent appComponent;


    @Override
    public void onCreate() {
        super.onCreate();

        MultiDex.install(this);
        FirebaseApp.initializeApp(this);
        FirebaseMessaging.getInstance().setAutoInitEnabled(true);

        YandexMetricaConfig config = YandexMetricaConfig.newConfigBuilder("7c0b7f05-45c1-401b-b520-cfab7ac1f1f6").build();
        YandexMetrica.activate(getApplicationContext(), config);
        YandexMetrica.enableActivityAutoTracking(this);

        this.appComponent = DaggerAppComponent.builder()
                .mainModule(new MainModule(this))
                .roomModule(new RoomModule(this))
                .build();

        appComponent.inject(this);


        initImageLoader(this);


    }

    public static void initImageLoader(Context context) {

        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .resetViewBeforeLoading()
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                .diskCacheSize(50 * 1024 * 1024) // 50 Mb
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .defaultDisplayImageOptions(options)
                .build();

        ImageLoader.getInstance().init(config);
    }



}
