package com.trablone.basket.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.LinearLayout;

import com.nostra13.universalimageloader.utils.L;
import com.trablone.basket.App;
import com.trablone.basket.di.Repository;
import com.trablone.basket.model.Auto;
import com.trablone.basket.model.Card;
import com.trablone.basket.model.Order;
import com.trablone.basket.model.Product;
import com.trablone.basket.model.User;
import com.trablone.basket.response.ListResponse;
import com.trablone.basket.response.ObjectResponse;

import java.util.HashMap;

import javax.inject.Inject;

import retrofit2.http.QueryMap;

public class UserViewModel extends AndroidViewModel {

    @Inject
    public Repository repository;

    public UserViewModel(@NonNull Application application) {
        super(application);
        ((App) application).getAppComponent().inject(this);
    }


    public LiveData<ObjectResponse> getUser() {
        final MutableLiveData<ObjectResponse> postsResponseMutableLiveData = new MutableLiveData<>();
        repository.getUser().observeForever(new Observer<ObjectResponse>() {
            @Override
            public void onChanged(@Nullable ObjectResponse postsResponse) {
                postsResponseMutableLiveData.postValue(postsResponse);
            }
        });
        return postsResponseMutableLiveData;
    }

    public LiveData<ObjectResponse> getGeo(double x, double y) {
        final MutableLiveData<ObjectResponse> postsResponseMutableLiveData = new MutableLiveData<>();
        repository.getGeo(x, y).observeForever(new Observer<ObjectResponse>() {
            @Override
            public void onChanged(@Nullable ObjectResponse postsResponse) {
                postsResponseMutableLiveData.postValue(postsResponse);
            }
        });
        return postsResponseMutableLiveData;
    }

    public LiveData<ObjectResponse> getMerchant() {
        final MutableLiveData<ObjectResponse> postsResponseMutableLiveData = new MutableLiveData<>();
        repository.getMerchant().observeForever(new Observer<ObjectResponse>() {
            @Override
            public void onChanged(@Nullable ObjectResponse postsResponse) {
                postsResponseMutableLiveData.postValue(postsResponse);
            }
        });
        return postsResponseMutableLiveData;
    }

    public LiveData<ObjectResponse> createOrder(int company, String order, @QueryMap HashMap<String, String> map) {
        final MutableLiveData<ObjectResponse> postsResponseMutableLiveData = new MutableLiveData<>();
        repository.createOrder(company, order, map).observeForever(new Observer<ObjectResponse>() {
            @Override
            public void onChanged(@Nullable ObjectResponse postsResponse) {
                postsResponseMutableLiveData.postValue(postsResponse);
            }
        });
        return postsResponseMutableLiveData;
    }

    public LiveData<ListResponse> getCards() {
        final MutableLiveData<ListResponse> postsResponseMutableLiveData = new MutableLiveData<>();
        repository.getCards().observeForever(new Observer<ListResponse>() {
            @Override
            public void onChanged(@Nullable ListResponse postsResponse) {
                postsResponseMutableLiveData.postValue(postsResponse);
            }
        });
        return postsResponseMutableLiveData;
    }

    public LiveData<ListResponse> getAutos() {
        final MutableLiveData<ListResponse> postsResponseMutableLiveData = new MutableLiveData<>();
        repository.getAutos().observeForever(new Observer<ListResponse>() {
            @Override
            public void onChanged(@Nullable ListResponse postsResponse) {
                postsResponseMutableLiveData.postValue(postsResponse);
            }
        });
        return postsResponseMutableLiveData;
    }

    public LiveData<ObjectResponse> register(String name, String phone) {
        final MutableLiveData<ObjectResponse> postsResponseMutableLiveData = new MutableLiveData<>();
        repository.register(name, phone).observeForever(new Observer<ObjectResponse>() {
            @Override
            public void onChanged(@Nullable ObjectResponse postsResponse) {
                postsResponseMutableLiveData.postValue(postsResponse);
            }
        });
        return postsResponseMutableLiveData;
    }

    public LiveData<ObjectResponse> confirmRegister(String sms, String phone, String token) {
        final MutableLiveData<ObjectResponse> postsResponseMutableLiveData = new MutableLiveData<>();
        repository.confirmRegister(sms, phone, token).observeForever(new Observer<ObjectResponse>() {
            @Override
            public void onChanged(@Nullable ObjectResponse postsResponse) {
                postsResponseMutableLiveData.postValue(postsResponse);
            }
        });
        return postsResponseMutableLiveData;
    }

    public LiveData<ObjectResponse> confirmRegister(String phone) {
        final MutableLiveData<ObjectResponse> postsResponseMutableLiveData = new MutableLiveData<>();
        repository.confirmRegister(phone).observeForever(new Observer<ObjectResponse>() {
            @Override
            public void onChanged(@Nullable ObjectResponse postsResponse) {
                postsResponseMutableLiveData.postValue(postsResponse);
            }
        });
        return postsResponseMutableLiveData;
    }

    public LiveData<ListResponse> getProductBasket(int company) {
        final MutableLiveData<ListResponse> postResponseMutableLiveData = new MutableLiveData<>();
        repository.getProductBasket(company).observeForever(new Observer<ListResponse>() {
            @Override
            public void onChanged(@Nullable ListResponse objectResponse) {
                postResponseMutableLiveData.postValue(objectResponse);
            }
        });
        return postResponseMutableLiveData;
    }


    public void insertUser(User account) {
        repository.insertUser(account);
    }

    public void insertAuto(Auto account) {
        repository.insertAuto(account);
    }

    public void deleteUser() {
        repository.deleteUser();
    }

    public void deleteAuto(Auto account) {
        repository.deleteAuto(account);
    }

    public void insertCard(Card account) {
        repository.insertCard(account);
    }

    public void insertOrder(Order account) {
        repository.insertOrder(account);
    }

    public void updateProduct(Product account) {
        repository.updateProduct(account);
    }

    public void deleteCard(Card account) {
        repository.deleteCard(account);
    }

    public void updateAuto(Auto account) {
        repository.updateAuto(account);
    }

}