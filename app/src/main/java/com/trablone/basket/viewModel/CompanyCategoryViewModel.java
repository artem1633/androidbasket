package com.trablone.basket.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.trablone.basket.App;
import com.trablone.basket.di.Repository;
import com.trablone.basket.response.ListResponse;
import com.trablone.basket.response.ObjectResponse;

import javax.inject.Inject;

public class CompanyCategoryViewModel extends AndroidViewModel {

    @Inject
    public Repository repository;

    public CompanyCategoryViewModel(@NonNull Application application) {
        super(application);
        ((App) application).getAppComponent().inject(this);
    }


    public LiveData<ObjectResponse> getCategory(int company) {
        final MutableLiveData<ObjectResponse> postsResponseMutableLiveData = new MutableLiveData<>();
        repository.getCategoryProductCompany(company).observeForever(new Observer<ObjectResponse>() {
            @Override
            public void onChanged(@Nullable ObjectResponse postsResponse) {
                postsResponseMutableLiveData.postValue(postsResponse);
            }
        });
        return postsResponseMutableLiveData;
    }

    public LiveData<ListResponse> getProductBasket(int company) {
        final MutableLiveData<ListResponse> postResponseMutableLiveData = new MutableLiveData<>();
        repository.getProductBasket(company).observeForever(new Observer<ListResponse>() {
            @Override
            public void onChanged(@Nullable ListResponse objectResponse) {
                postResponseMutableLiveData.postValue(objectResponse);
            }
        });
        return postResponseMutableLiveData;
    }


}
