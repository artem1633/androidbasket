package com.trablone.basket.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.trablone.basket.App;
import com.trablone.basket.di.Repository;
import com.trablone.basket.model.Product;
import com.trablone.basket.response.ObjectResponse;

import javax.inject.Inject;

public class OrderHistoryViewModel extends AndroidViewModel {

    @Inject
    public Repository repository;

    public OrderHistoryViewModel(@NonNull Application application) {
        super(application);
        ((App) application).getAppComponent().inject(this);
    }

    public LiveData<ObjectResponse> getOrders(int limit, int offset) {
        final MutableLiveData<ObjectResponse> postsResponseMutableLiveData = new MutableLiveData<>();
        repository.getOrderHistory(limit, offset).observeForever(new Observer<ObjectResponse>() {
            @Override
            public void onChanged(@Nullable ObjectResponse postsResponse) {
                postsResponseMutableLiveData.postValue(postsResponse);
            }
        });
        return postsResponseMutableLiveData;
    }



    public void updateProduct(Product account){
        repository.updateProduct(account);
    }

}

