package com.trablone.basket.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.trablone.basket.App;
import com.trablone.basket.di.Repository;
import com.trablone.basket.model.Product;
import com.trablone.basket.response.ListResponse;
import com.trablone.basket.response.ObjectResponse;

import javax.inject.Inject;

public class ProductViewModel extends AndroidViewModel {

    @Inject
    public Repository repository;

    public ProductViewModel(@NonNull Application application) {
        super(application);
        ((App) application).getAppComponent().inject(this);
    }


    public LiveData<ObjectResponse> getProducts(int category, int company) {
        final MutableLiveData<ObjectResponse> postsResponseMutableLiveData = new MutableLiveData<>();
        repository.getProducts(category,company).observeForever(new Observer<ObjectResponse>() {
            @Override
            public void onChanged(@Nullable ObjectResponse postsResponse) {
                postsResponseMutableLiveData.postValue(postsResponse);
            }
        });
        return postsResponseMutableLiveData;
    }



    public void updateProduct(Product account){
        repository.updateProduct(account);
    }

}
