package com.trablone.basket.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.trablone.basket.App;
import com.trablone.basket.di.Repository;
import com.trablone.basket.model.Product;
import com.trablone.basket.response.ObjectResponse;

import javax.inject.Inject;

public class SupportViewModel extends AndroidViewModel {

    @Inject
    public Repository repository;

    public SupportViewModel(@NonNull Application application) {
        super(application);
        ((App) application).getAppComponent().inject(this);
    }

    public LiveData<ObjectResponse> getSupport() {
        final MutableLiveData<ObjectResponse> postsResponseMutableLiveData = new MutableLiveData<>();
        repository.getSupport().observeForever(new Observer<ObjectResponse>() {
            @Override
            public void onChanged(@Nullable ObjectResponse postsResponse) {
                postsResponseMutableLiveData.postValue(postsResponse);
            }
        });
        return postsResponseMutableLiveData;
    }



}
