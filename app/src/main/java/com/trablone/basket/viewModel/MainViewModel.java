package com.trablone.basket.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.trablone.basket.App;
import com.trablone.basket.di.Repository;
import com.trablone.basket.model.Auto;
import com.trablone.basket.model.Card;
import com.trablone.basket.model.Order;
import com.trablone.basket.response.ListResponse;
import com.trablone.basket.response.ObjectResponse;

import java.util.HashMap;

import javax.inject.Inject;

public class MainViewModel extends AndroidViewModel {

    @Inject
    public Repository repository;

    public MainViewModel(@NonNull Application application) {
        super(application);
        ((App) application).getAppComponent().inject(this);
    }

    public LiveData<ObjectResponse> getAllCompanies() {
        final MutableLiveData<ObjectResponse> postsResponseMutableLiveData = new MutableLiveData<>();
        repository.getAllCompany().observeForever(new Observer<ObjectResponse>() {
            @Override
            public void onChanged(@Nullable ObjectResponse postsResponse) {
                postsResponseMutableLiveData.postValue(postsResponse);
            }
        });
        return postsResponseMutableLiveData;
    }

    public LiveData<ObjectResponse> getGeo(double x, double y) {
        final MutableLiveData<ObjectResponse> postsResponseMutableLiveData = new MutableLiveData<>();
        repository.getGeo(x, y).observeForever(new Observer<ObjectResponse>() {
            @Override
            public void onChanged(@Nullable ObjectResponse postsResponse) {
                postsResponseMutableLiveData.postValue(postsResponse);
            }
        });
        return postsResponseMutableLiveData;
    }

    public LiveData<ObjectResponse> getAllCompanyCategory(int cat) {
        final MutableLiveData<ObjectResponse> postsResponseMutableLiveData = new MutableLiveData<>();
        repository.getAllCompanyCategory(cat).observeForever(new Observer<ObjectResponse>() {
            @Override
            public void onChanged(@Nullable ObjectResponse postsResponse) {
                postsResponseMutableLiveData.postValue(postsResponse);
            }
        });
        return postsResponseMutableLiveData;
    }



    public LiveData<ObjectResponse> getCategoryCompany() {
        final MutableLiveData<ObjectResponse> postsResponseMutableLiveData = new MutableLiveData<>();
        repository.getCategoryCompany().observeForever(new Observer<ObjectResponse>() {
            @Override
            public void onChanged(@Nullable ObjectResponse postsResponse) {
                postsResponseMutableLiveData.postValue(postsResponse);
            }
        });
        return postsResponseMutableLiveData;
    }


    public LiveData<ObjectResponse> getRoutes(HashMap<String, String> map) {
        final MutableLiveData<ObjectResponse> postsResponseMutableLiveData = new MutableLiveData<>();
        repository.getRoutes(map).observeForever(new Observer<ObjectResponse>() {
            @Override
            public void onChanged(@Nullable ObjectResponse postsResponse) {
                postsResponseMutableLiveData.postValue(postsResponse);
            }
        });
        return postsResponseMutableLiveData;
    }

    public LiveData<ObjectResponse> getOrderStatus(int order_id) {
        final MutableLiveData<ObjectResponse> postsResponseMutableLiveData = new MutableLiveData<>();
        repository.getOrderStatus(order_id).observeForever(new Observer<ObjectResponse>() {
            @Override
            public void onChanged(@Nullable ObjectResponse postsResponse) {
                postsResponseMutableLiveData.postValue(postsResponse);
            }
        });
        return postsResponseMutableLiveData;
    }

    public void updateOrder(Order order) {
        repository.updateOrder(order);
    }
    public LiveData<ListResponse> getOrders() {
        final MutableLiveData<ListResponse> postsResponseMutableLiveData = new MutableLiveData<>();
        repository.getOrders().observeForever(new Observer<ListResponse>() {
            @Override
            public void onChanged(@Nullable ListResponse postsResponse) {
                postsResponseMutableLiveData.postValue(postsResponse);
            }
        });
        return postsResponseMutableLiveData;
    }

    public void deleteOrder(Order account) {
        repository.deleteOrder(account);
    }

}