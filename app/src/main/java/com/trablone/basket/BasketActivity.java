package com.trablone.basket;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.trablone.basket.adapters.BasketAdapter;
import com.trablone.basket.model.Company;
import com.trablone.basket.model.Product;
import com.trablone.basket.response.ListResponse;
import com.trablone.basket.viewModel.BasketViewModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BasketActivity extends AppCompatActivity {

    public static void showActivity(Context context, Company company){
        Intent intent = new Intent(context, BasketActivity.class);
        intent.putExtra("company", company);
        context.startActivity(intent);
    }

    @BindView(R.id.item_sum)
    TextView textSum;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recycler_product)
    RecyclerView recyclerView;
    private BasketViewModel viewModel;

    private BasketAdapter adapter;

    public Company getCompany() {
        return company;
    }

    private List<Product> list;
    private Company company;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basket);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        company = (Company) getIntent().getSerializableExtra("company");

        viewModel = ViewModelProviders.of(this).get(BasketViewModel.class);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new BasketAdapter(this, company, item -> {
            viewModel.updateProduct(item);
            calcSum();
        });

        recyclerView.setAdapter(adapter);

        viewModel.getProductBasket(company.getId()).observe(this, listResponse -> {
            list = (List<Product>)listResponse.getList();
            Log.e("tr", "count basket " + list.size());
            adapter.setList(list);
            calcSum();
        });

    }

    private int price = 0;

    private void calcSum(){
        int sum = 0;
        for (Product product : list){
            sum += (product.getCount() * Integer.parseInt(product.getPrice()));
        }

        price = sum;
        textSum.setText(String.valueOf(sum));
    }

    @OnClick(R.id.item_show_confirn)
    public void showConfirm(){
        ActivityConfirmOrder.showActivity(this, price, company);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
