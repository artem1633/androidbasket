package com.trablone.basket;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.trablone.basket.model.Auto;
import com.trablone.basket.model.User;
import com.trablone.basket.phonemask.MaskEditText;
import com.trablone.basket.response.ObjectResponse;
import com.trablone.basket.rest.GetConfirmRegister;
import com.trablone.basket.rest.GetRegister;
import com.trablone.basket.viewModel.UserViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.item_name)
    EditText editName;
    @BindView(R.id.item_sms)
    EditText editSms;
    @BindView(R.id.item_phone)
    MaskEditText editPhone;
    @BindView(R.id.item_login)
    TextView butonLogin;
    @BindView(R.id.item_register)
    TextView butonRegister;
    @BindView(R.id.button_sms)
    TextView buttonSms;

    private UserViewModel viewModel;

    private boolean ignoreTextChange = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        viewModel = ViewModelProviders.of(this).get(UserViewModel.class);

        /*butonLogin.setEnabled(false);
        editPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!ignoreTextChange) {
                    ignoreTextChange = true;
                    String phoneNumber = s.toString().replaceAll("[^0-9-+]", "");
                    Log.e("tr", "change:" + phoneNumber + " " + phoneNumber.length());
                    butonLogin.setEnabled(phoneNumber.length() == 12 && !TextUtils.isEmpty(editSms.getText().toString()));
                    ignoreTextChange = false;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });*/
    }

    private boolean register = false;

    @OnClick(R.id.item_login)
    public void login() {
        hideKeyboard();
        String phone = editPhone.getText().toString().replaceAll("[^0-9-+]+", "");
        String name = editName.getText().toString();

        String smsEdit = editSms.getText().toString();

        if (TextUtils.isEmpty(phone)) {
            editPhone.setError("введите телефон");
            return;
        }

        if (!register) {


            if (TextUtils.isEmpty(smsEdit)) {
                editSms.setError("введите код из смс");
                return;
            }
            //5371

            String refreshedToken = FirebaseInstanceId.getInstance().getToken();

            viewModel.confirmRegister(smsEdit, phone, refreshedToken).observeForever(new Observer<ObjectResponse>() {
                @Override
                public void onChanged(@Nullable ObjectResponse objectResponse) {
                    if (objectResponse.getObject() != null) {
                        GetConfirmRegister register = (GetConfirmRegister) objectResponse.getObject();
                        if (register.isStatus()) {
                            User user = new User();
                            user.setId(1);
                            //user.setName(name);
                            user.setPhone(phone);
                            user.setAccess(register.getAccess());
                            viewModel.insertUser(user);


                            if (!TextUtils.isEmpty(register.getCar_color()) && !TextUtils.isEmpty(register.getCar_type()) && !TextUtils.isEmpty(register.getCar_number())) {
                                Auto auto = new Auto();
                                auto.setAutoNumber(Integer.valueOf(register.getCar_number()));
                                auto.setAutoMark(register.getCar_type());
                                auto.setAutoColor(register.getCar_color());
                                if (!TextUtils.isEmpty(register.getCar_model()))
                                    auto.setAutoModel(register.getCar_model());
                                viewModel.insertAuto(auto);
                            }
                            MainActivity.showActivity(LoginActivity.this);
                            finish();
                        } else {
                            Snackbar.make(butonLogin, register.getErrors(), Snackbar.LENGTH_SHORT).show();
                        }
                    }
                }
            });
        } else {

            if (TextUtils.isEmpty(name)) {
                editName.setError("введите имя");
                return;
            }
            viewModel.register(name, phone).observeForever(new Observer<ObjectResponse>() {
                @Override
                public void onChanged(@Nullable ObjectResponse objectResponse) {
                    if (objectResponse.getObject() != null) {
                        GetRegister register = (GetRegister) objectResponse.getObject();
                        if (register.isStatus()) {
                            LoginActivity.this.register = false;
                            editName.setVisibility(View.GONE);
                            editSms.setVisibility(View.VISIBLE);
                            editSms.setText(register.getClientSMS());
                            butonLogin.setText("Войти");
                            butonRegister.setVisibility(View.VISIBLE);
                        } else {
                            Snackbar.make(butonLogin, register.getErrors(), Snackbar.LENGTH_SHORT).show();
                        }
                    }
                }
            });
        }
    }

    @OnClick(R.id.button_sms)
    public void sms() {

        hideKeyboard();
        editSms.setVisibility(View.GONE);
        editName.setVisibility(View.GONE);
        butonLogin.setVisibility(View.GONE);
        butonRegister.setVisibility(View.GONE);

        String phone = editPhone.getText().toString().replaceAll("[^0-9-+]+", "");

        Log.e("tr", "phone " + phone);
        if (TextUtils.isEmpty(phone)) {
            editPhone.setError("введите телефон");
            return;
        }


        viewModel.confirmRegister(phone).observeForever(new Observer<ObjectResponse>() {
            @Override
            public void onChanged(@Nullable ObjectResponse objectResponse) {
                if (objectResponse.getObject() != null) {
                    GetConfirmRegister register = (GetConfirmRegister) objectResponse.getObject();
                    if (register.isStatus()) {
                        editSms.setVisibility(View.VISIBLE);
                        butonLogin.setVisibility(View.VISIBLE);
                        butonRegister.setVisibility(View.VISIBLE);
                    } else {
                        Snackbar.make(butonLogin, register.getErrors(), Snackbar.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }

    @OnClick(R.id.item_register)
    public void register() {
        hideKeyboard();
        editName.setVisibility(View.VISIBLE);
        editSms.setVisibility(View.GONE);
        buttonSms.setVisibility(View.GONE);

        register = true;
        butonLogin.setText("Регистрация");
        //butonLogin.setEnabled(true);
        butonRegister.setVisibility(View.GONE);
    }

    protected void hideKeyboard() {
        try {
            View currentFocus = getCurrentFocus();
            if (currentFocus != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (inputMethodManager != null) {
                    inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 0);
                }
            }
        } catch (Exception e) {

        }
    }
}
