package com.trablone.basket.adapters;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.trablone.basket.fragment.ProductListFragment;
import com.trablone.basket.model.CategoryCompany;

import java.util.List;

public class CategoryPagerAdapter extends FragmentStatePagerAdapter {

    private List<CategoryCompany> list;
    private int company_id;
    public CategoryPagerAdapter(FragmentManager fm, List<CategoryCompany> list, int company_id) {
        super(fm);
        this.list = list;
        this.company_id = company_id;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return list.get(position).getName();
    }

    @Override
    public Fragment getItem(int position) {
        return ProductListFragment.getInstance(list.get(position).getId(), company_id);
    }

    @Override
    public int getCount() {
        if (list != null)
        return list.size();

        return 0;
    }
}
