package com.trablone.basket.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.trablone.basket.R;
import com.trablone.basket.model.CategoryCompany;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoryCompanyAdapter extends RecyclerView.Adapter<CategoryCompanyAdapter.ViewHolder>{

    public List<CategoryCompany> getList() {
        return list;
    }

    public void setList(List<CategoryCompany> list) {
        this.list = list;
        notifyDataSetChanged();
    }



    private List<CategoryCompany> list;
    private Context context;
    private CategoryCompanyListener listener;

    public CategoryCompanyAdapter(Context context, CategoryCompanyListener listener) {
        this.context = context;
        this.listener = listener;
        list = new ArrayList<>();
    }

    public interface CategoryCompanyListener{
        void onCategoryCompany(CategoryCompany item);
    }

    public boolean isChecked(){
        for (CategoryCompany item : list){
            if (item.isCheck()){
                return true;
            }
        }

        return false;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_category_company, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        final CategoryCompany item = list.get(position);
        holder.textView.setText(item.getName());
        if (item.isCheck()){
            holder.textView.setBackgroundResource(R.drawable.background_item_select_category);
        }else {
            holder.textView.setBackgroundResource(R.drawable.button_black);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item.setCheck(!item.isCheck());
                listener.onCategoryCompany(item);
                notifyItemChanged(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.item_title)
        TextView textView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
