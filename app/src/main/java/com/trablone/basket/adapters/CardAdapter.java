package com.trablone.basket.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.trablone.basket.R;
import com.trablone.basket.model.Auto;
import com.trablone.basket.model.Card;
import com.trablone.basket.payment.googlepay.Constants;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.cloudpayments.cpcard.CPCard;
import ru.cloudpayments.cpcard.CPCardFactory;

public class CardAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    public List<Card> getList() {
        return list;
    }

    private boolean set;
    public void setList(List<Card> list) {
        this.list = list;
        set = true;
        notifyDataSetChanged();
    }

    private List<Card> list;
    private AutoListener autoListener;
    private Context context;

    public CardAdapter(Context context, AutoListener autoListener) {
        this.context = context;
        this.autoListener = autoListener;
        list = new ArrayList<>();
    }

    public interface AutoListener{
        void addAuto(Card auto);
        void deleteAuto(Card auto);
    }

    private int selection = 1;
    private void setSelection(int position){
        this.selection = position;
        notifyDataSetChanged();
    }

    public Card getSelectAuto(){
        return list.get(selection - 1);
    }

    @Override
    public int getItemViewType(int position) {
        return position == 0 ? 0 : 1;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == 0){
            View view = LayoutInflater.from(context).inflate(R.layout.item_card_header, parent, false);
            return new ViewHolderHeader(view);
        }else {
            View view = LayoutInflater.from(context).inflate(R.layout.item_auto, parent, false);
            return new ViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderBase, final int position) {
        if (position == 0){
            final ViewHolderHeader holder = (ViewHolderHeader)holderBase;
            if (list.size() == 0 && set){
                holder.layoutData.setVisibility(View.VISIBLE);

            }else {
                holder.layoutData.setVisibility(View.GONE);
            }

            holder.layoutExpand.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (holder.layoutData.getVisibility() == View.VISIBLE){
                        holder.layoutData.setVisibility(View.GONE);
                        holder.imageExpand.setImageResource(R.drawable.ic_expand_more_black_24dp);
                    }else {
                        holder.layoutData.setVisibility(View.VISIBLE);
                        holder.imageExpand.setImageResource(R.drawable.ic_expand_less_black_24dp);
                    }
                }
            });

            holder.textAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String cardNumber = holder.editTextCardNumber.getText().toString();
                    String cardExp = holder.editTextCardExp.getText().toString();
                    String cardHolderName = holder.editTextCardHolderName.getText().toString();
                    String cardCVV = holder.editTextCardCVV.getText().toString();



                    CPCard card = CPCardFactory.create(cardNumber, cardExp, cardCVV);

                    // Создаем криптограмму карточных данных
                    String cardCryptogram = null;
                    try {
                        // Чтобы создать криптограмму необходим PublicID (его можно посмотреть в личном кабинете)
                        cardCryptogram = card.cardCryptogram(Constants.MERCHANT_PUBLIC_ID);

                        Log.e("tr", "hash " + cardCryptogram);
                        final Card auto = new Card();
                        auto.setId(list.size() + 1);
                        auto.setHash(cardCryptogram);
                        auto.setHolderName(cardHolderName);
                        auto.setName("**** **** **** " + cardNumber.substring(12));

                        autoListener.addAuto(auto);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (NoSuchPaddingException e) {
                        e.printStackTrace();
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    } catch (NoSuchProviderException e) {
                        e.printStackTrace();
                    } catch (BadPaddingException e) {
                        e.printStackTrace();
                    } catch (IllegalBlockSizeException e) {
                        e.printStackTrace();
                    } catch (InvalidKeyException e) {
                        e.printStackTrace();
                    } catch (StringIndexOutOfBoundsException e) {
                        e.printStackTrace();
                    }
                }
            });
        }else {
            final ViewHolder holder = (ViewHolder)holderBase;
            final Card item = list.get(position - 1);
            holder.textView.setText(item.getName());
            if (selection == position)
                holder.textView.setChecked(true);
            else {
                holder.textView.setChecked(false);
            }

            holder.imageDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    autoListener.deleteAuto(item);
                }
            });
            holder.textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setSelection(position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return list.size() + 1;
    }

    public class ViewHolderHeader extends RecyclerView.ViewHolder{

        @BindView(R.id.edit_card_number)
        EditText editTextCardNumber;

        @BindView(R.id.edit_card_exp)
        EditText editTextCardExp;

        @BindView(R.id.edit_card_holder_name)
        EditText editTextCardHolderName;

        @BindView(R.id.edit_card_cvv)
        EditText editTextCardCVV;
        @BindView(R.id.item_add)
        TextView textAdd;

        @BindView(R.id.layout_expand)
        LinearLayout layoutExpand;
        @BindView(R.id.item_expand)
        ImageView imageExpand;
        @BindView(R.id.layout_data)
        LinearLayout layoutData;


        public ViewHolderHeader(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.item_title)
        CheckBox textView;
        @BindView(R.id.item_delete)
        ImageView imageDelete;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
