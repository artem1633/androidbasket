package com.trablone.basket.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.trablone.basket.R;
import com.trablone.basket.model.Auto;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AutoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    public List<Auto> getList() {
        return list;
    }

    public void setList(List<Auto> list) {
        this.list = list;
        set = true;
        notifyDataSetChanged();
    }

    private boolean set;

    private List<Auto> list;
    private AutoListener autoListener;
    private Context context;

    public AutoAdapter(Context context,AutoListener autoListener) {
        this.context = context;
        this.autoListener = autoListener;
        list = new ArrayList<>();
    }

    public interface AutoListener{
        void addAuto(Auto auto);
        void deleteAuto(Auto auto);
    }

    private int selection = 1;
    private void setSelection(int position){
        this.selection = position;
        notifyDataSetChanged();
    }

    public Auto getSelectAuto(){
        return list.get(selection - 1);
    }

    @Override
    public int getItemViewType(int position) {
        return position == 0 ? 0 : 1;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == 0){
            View view = LayoutInflater.from(context).inflate(R.layout.iteh_auto_header, parent, false);
            return new ViewHolderHeader(view);
        }else {
            View view = LayoutInflater.from(context).inflate(R.layout.item_auto, parent, false);
            return new ViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderBase, final int position) {
        if (position == 0){
            final ViewHolderHeader holder = (ViewHolderHeader)holderBase;
            if (list.size() == 0 && set){
                holder.layoutData.setVisibility(View.VISIBLE);
            }else {
                holder.layoutData.setVisibility(View.GONE);
            }

            holder.layoutExpand.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (holder.layoutData.getVisibility() == View.VISIBLE){
                        holder.layoutData.setVisibility(View.GONE);
                        holder.imageExpand.setImageResource(R.drawable.ic_expand_more_black_24dp);
                    }else {
                        holder.layoutData.setVisibility(View.VISIBLE);
                        holder.imageExpand.setImageResource(R.drawable.ic_expand_less_black_24dp);
                    }
                }
            });
            holder.textAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String color = holder.editColor.getText().toString();
                    String mark = holder.editMark.getText().toString();
                    String model = holder.editModel.getText().toString();
                    String number = holder.editNumber.getText().toString();

                    final Auto auto = new Auto();
                    if (list != null && list.size() != 0)
                        auto.setId(list.get(list.size() - 1).getId() + 1);
                    else {
                        auto.setId(1);
                    }
                    if (!TextUtils.isEmpty(color) && !TextUtils.isEmpty(mark) && !TextUtils.isEmpty(number)){
                        auto.setAutoColor(color);
                        auto.setAutoMark(mark);
                        auto.setAutoModel(model);
                        auto.setAutoNumber(Integer.parseInt(number));
                        autoListener.addAuto(auto);
                    }else {
                        Toast.makeText(context, "Заполните все поля", Toast.LENGTH_SHORT).show();
                    }





                }
            });
        }else {
            final ViewHolder holder = (ViewHolder)holderBase;
            final Auto item = list.get(position - 1);
            holder.textView.setText(item.getAuto());
            if (selection == position)
                holder.textView.setChecked(true);
            else {
                holder.textView.setChecked(false);
            }

            holder.imageDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    autoListener.deleteAuto(item);
                }
            });
            holder.textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setSelection(position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return list.size() + 1;
    }

    public class ViewHolderHeader extends RecyclerView.ViewHolder{

        @BindView(R.id.item_auto_mark)
        EditText editMark;
        @BindView(R.id.item_auto_model)
        EditText editModel;
        @BindView(R.id.item_auto_color)
        EditText editColor;
        @BindView(R.id.item_auto_number)
        EditText editNumber;
        @BindView(R.id.item_add)
        TextView textAdd;

        @BindView(R.id.layout_expand)
        LinearLayout layoutExpand;
        @BindView(R.id.item_expand)
        ImageView imageExpand;
        @BindView(R.id.layout_data)
        LinearLayout layoutData;


        public ViewHolderHeader(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.item_title)
        CheckBox textView;
        @BindView(R.id.item_delete)
        ImageView imageDelete;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
