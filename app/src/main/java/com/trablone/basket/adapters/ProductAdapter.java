package com.trablone.basket.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.trablone.basket.R;
import com.trablone.basket.di.MainModule;
import com.trablone.basket.model.CategoryCompany;
import com.trablone.basket.model.Product;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    public List<Product> getList() {
        return list;
    }

    public void setList(List<Product> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    private List<Product> list;
    private Context context;
    private ProductListener listener;

    private ImageLoader imageLoader = ImageLoader.getInstance();

    public ProductAdapter(Context context, ProductListener listener) {
        this.context = context;
        this.listener = listener;
        list = new ArrayList<>();
    }

    public interface ProductListener {
        void onUpdate(Product item);
    }

    @Override
    public int getItemViewType(int position) {
        return position == list.size() ? 1 : 0;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == 0){
            View view = LayoutInflater.from(context).inflate(R.layout.item_product, parent, false);
            return new ViewHolder(view);
        }else {
            View view = LayoutInflater.from(context).inflate(R.layout.item_footer, parent, false);
            return new ViewHolderFooter(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderBase, final int position) {
        if (position < list.size()){
            final ViewHolder holder = (ViewHolder)holderBase;
            final Product item = list.get(position);
            holder.textView.setText(item.getName());
            holder.textDesc.setText(item.getDescription());
            holder.textPrice.setText(item.getPrice());
            imageLoader.displayImage(MainModule.BASE_URL + item.getPicture(), holder.imageView);

            if (item.getCount() == 0){
                holder.layoutAdd.setVisibility(View.VISIBLE);
                holder.layoutCounts.setVisibility(View.GONE);
                holder.textAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        item.setCount(1);
                        listener.onUpdate(item);
                        notifyItemChanged(position);
                    }
                });
            }else {
                holder.textCount.setText(String.valueOf(item.getCount()));
                holder.layoutAdd.setVisibility(View.GONE);
                holder.layoutCounts.setVisibility(View.VISIBLE);
                holder.textPlus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        item.setCount(item.getCount() + 1);
                        listener.onUpdate(item);
                        notifyItemChanged(position);
                    }
                });

                holder.textMinus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (item.getCount() > 0) {
                            item.setCount(item.getCount() - 1);
                            listener.onUpdate(item);
                            notifyItemChanged(position);
                        }
                    }
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        return list.size() + 1;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.item_title)
        TextView textView;
        @BindView(R.id.item_desc)
        TextView textDesc;
        @BindView(R.id.item_price)
        TextView textPrice;
        @BindView(R.id.item_image)
        ImageView imageView;
        @BindView(R.id.layout_add)
        LinearLayout layoutAdd;
        @BindView(R.id.layout_counts)
        LinearLayout layoutCounts;
        @BindView(R.id.item_minus)
        ImageView textMinus;
        @BindView(R.id.item_plus)
        ImageView textPlus;
        @BindView(R.id.item_count)
        TextView textCount;
        @BindView(R.id.item_add)
        TextView textAdd;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public class ViewHolderFooter extends RecyclerView.ViewHolder{

        public ViewHolderFooter(View itemView) {
            super(itemView);

        }
    }
}