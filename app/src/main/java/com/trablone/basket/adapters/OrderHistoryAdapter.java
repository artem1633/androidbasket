package com.trablone.basket.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.trablone.basket.R;
import com.trablone.basket.model.OrderHistrory;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderHistoryAdapter extends RecyclerView.Adapter<OrderHistoryAdapter.ViewHolder>{

    private Context context;
    private List<OrderHistrory> list;

    public OrderHistoryAdapter(Context context) {
        this.context = context;
        list = new ArrayList<>();
    }

    public List<OrderHistrory> getList() {
        return list;
    }

    public void setList(List<OrderHistrory> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void addList(List<OrderHistrory> list) {
        this.list.addAll(list);
        notifyDataSetChanged();

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_order_history, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final OrderHistrory item = list.get(position);
        holder.textOrderPrice.setText(item.getSumma());
        holder.textOrderStatus.setText(getOrderStatus(item.getStatus()));
        holder.textOrderTime.setText(item.getOrder_date());
    }

    private String getOrderStatus(String status) {
        switch (status) {
            case "1":
                return "Новый";
            case "2":
                return "В работе";
            case "3":
                return "В доставке";
            case "4":
                return "Выполнен";
            default:
                return "Предварительный";
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.item_order_price)
        TextView textOrderPrice;
        @BindView(R.id.item_order_time)
        TextView textOrderTime;
        @BindView(R.id.item_order_status)
        TextView textOrderStatus;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
