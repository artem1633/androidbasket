package com.trablone.basket;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.trablone.basket.adapters.CategoryPagerAdapter;
import com.trablone.basket.di.MainModule;
import com.trablone.basket.model.CategoryCompany;
import com.trablone.basket.model.Company;
import com.trablone.basket.model.Product;
import com.trablone.basket.response.ListResponse;
import com.trablone.basket.response.ObjectResponse;
import com.trablone.basket.rest.GetCategoryProduct;
import com.trablone.basket.viewModel.CompanyCategoryViewModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CompanyActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.toolbarImage)
    ImageView imageView;

    private Company company;

    @BindView(R.id.item_basket)
    CardView cardBasket;
    @BindView(R.id.item_basket_count)
    TextView textBasketCount;
    @BindView(R.id.item_basket_price)
    TextView textBasketPrice;

    private boolean basket;

    private CompanyCategoryViewModel viewModel;
    private CategoryPagerAdapter adapter;
    private ImageLoader imageLoader = ImageLoader.getInstance();

    public static void showActivity(Context context, Company company){
        Intent intent = new Intent(context, CompanyActivity.class);
        intent.putExtra("company", company);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        company = (Company) getIntent().getSerializableExtra("company");
        actionBar.setTitle(company.getName());
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        imageLoader.displayImage(MainModule.BASE_URL + company.getPicture(), imageView);
        viewModel = ViewModelProviders.of(this).get(CompanyCategoryViewModel.class);

        viewModel.getCategory(company.getId()).observeForever(new Observer<ObjectResponse>() {
            @Override
            public void onChanged(@Nullable ObjectResponse objectResponse) {
                if (objectResponse.getObject() != null){
                    GetCategoryProduct categoryProduct = (GetCategoryProduct)objectResponse.getObject();
                    List<CategoryCompany> list = categoryProduct.getCategoryProductList();
                    adapter = new CategoryPagerAdapter(getSupportFragmentManager(), list, company.getId());
                    viewPager.setAdapter(adapter);
                    tabLayout.setupWithViewPager(viewPager);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateBasket();
    }

    public void updateBasket(){
        viewModel.getProductBasket(company.getId()).observeForever(listResponse -> {
            List<Product> list = (List<Product>)listResponse.getList();
            Log.e("tr", "count company " + list.size());
            int count = 0;
            int price = 0;

            for (Product item : list){
                count += item.getCount();
                price += Integer.parseInt(item.getPrice()) * item.getCount();
            }

            textBasketCount.setText(String.valueOf(count));
            textBasketPrice.setText(String.valueOf(price));

            if (list.size() > 0){
                cardBasket.setVisibility(View.VISIBLE);
            }else {
                cardBasket.setVisibility(View.GONE);
            }
        });
    }

    @OnClick(R.id.item_basket)
    public void showBasket(){
        BasketActivity.showActivity(this, company);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }



}
